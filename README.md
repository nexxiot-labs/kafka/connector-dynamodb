Nexxiot's Kafka connector for DynamoDB
======================================

A [Kafka Connect][kafka-connect] source connector for [DynamoDB][dynamodb] based on
[DynamoDB Streams][dynamodb-streams]. DynamoDB Streams captures DynamoDB table changes
and exposes it as a stream of change events. You can use this connector to put all
captured changes to Kafka topic.

[kafka-connect]: https://kafka.apache.org/documentation/#connect
[dynamodb]: https://aws.amazon.com/dynamodb/
[dynamodb-streams]: https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/Streams.html


Functionality
-------------

### Output format

Captured records are returned to Kafka Connect as key-value pairs, where:

* Key is table's partition key returned as string. Range key (if any) is currently
  ignored. We currently support partition keys which are either strings or numbers.
  We don't support binary partition keys at the moment.
* Value is DynamoDB Streams event (see details below) represented as Kafka Connect
  Struct.

Example value (represented as JSON): 

```json
{
  "id": "f3db054f155a3b031c7d387a7c133341",
  "approx_time": "2019-11-04T12:51:46Z",
  "type": "MODIFY",
  "keys": "{\"id\":\"john\"}",
  "new_image": "{\"id\":\"john\",\"name\":\"John\"}",
  "old_image": "{\"id\":\"john\",\"name\":\"William\"}",
  "discover_time": "2019-11-04T12:51:47Z"
}
```

The values are mapped from [DynamoDB Record][stream-record] as follows:

* `id` is `eventID`
* `approx_time` is `dynamodb.ApproximateCreationDateTime` in ISO 8601 format in UTC.
* `type` is `eventName`
* `keys` is `dynamodb.Keys` converted to JSON object, where attribute names are
  object keys.  
* `new_image` is `dynamodb.NewImage` converted to JSON object, where attribute names
  are object keys. Availability depends on stream's view type configuration and
  event's `type`. 
* `old_image` is `dynamodb.OldImage` converted to JSON object, where attribute names
  are object keys. Availability depends on stream's view type configuration and
  event's `type`. It is returned as JSON without any schema, as getting schema is tricky -
  there is no standard way to share DynamoDB table's schema nor DynamoDB enforces
  any schema on write.
* `discover_time` is time, when the connector captured record.

The fields `keys`, `new_image` and `old_image` are returned as JSON string, without any
schema, as getting schema is tricky (for `new_image` and `old_image` fields) - there is
no standard way to share DynamoDB table's schema nor DynamoDB enforces any schema on
write. When using Kafka's JsonConverter, the entire value is serialized to JSON String,
as Kafka's JsonConverter has no support for arbitrary JSON objects. We do however set
field's logical type to `"com.nexxiot.kafka.connect.data.Json"` to give option to
serialize it as JSON object instead.

[stream-record]: https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_streams_Record.html


### Ordering and delivery semantics

The connector processes data following lineage of shards, that is child shards are
consumed only after all records from their parent shards are retrieved. This ensures,
that all changes per key are in order they happened. There are however 2 gotchas:

* Kafka Connect (at least versions 2.3 and before) has _"at least once"_ delivery,
  meaning that none of the records will be lost, but you can observe a sequence like
  1-2-3-2-3 where record with time 2 appears again after record with time 3 (the same
  is true for KCL though).
* If your Kafka Connect has `max.in.flight.requests.per.connection` set to anything
  greater than `1`, there is a risk of message re-ordering due to retries. 


Usage
-----

As prerequisite, you need to have:

* Kafka Connect cluster with this [connector already installed][connect-plugin-installation]
  and AWS credentials available in the [Default Credential Provider Chain][aws-credentials]. 
* DynamoDB table, which changes you want to capture.
* Kafka topic, to which changes will be written (see [KIP-158][kip-158] for
  details).

Having above, you need to:

1. Enable stream on the table. It is recommended that you use _New and Old
   Images_ view, as it will enable all the cases that may appear around
   consuming table's change stream.
2. Get the table's latest stream ARN (you can check it for example in AWS
   DynamoDB Console in table's overview).
3. Grant Kafka Connect IAM permissions to access the stream (or all streams
   for the table). The connector uses following actions: _DescribeStream_,
   _GetShardIterator_ and _GetRecords_ (see [DynamoDB API Permissions][dynamodb-iam]).
4. Create connector in Kafka Connect similar to below:

```
curl -X POST -H "Accept:application/json" -H "Content-Type:application/json" localhost:80/connectors/ -d '
{
 "name": "dynamodb-mytable",
 "config": {
   "connector.class": "com.nexxiot.kafka.connect.dynamodb.DynamoDbSourceConnector",
   "tasks.max": "1",
   "key.converter": "org.apache.kafka.connect.storage.StringConverter",
   "value.converter": "org.apache.kafka.connect.json.JsonConverter",
   "streams.arn": "arn:aws:dynamodb:eu-central-1:1234456789000:table/MyTable/stream/2019-11-01T00:00:00.000",
   "kafka.topic": "dynamodb.mytable.stream",
   "streams.get.records.min.interval.ms": 500
 }
}'
```


[connect-plugin-installation]: https://docs.confluent.io/current/connect/userguide.html#connect-installing-plugins
[dynamodb-iam]: https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/api-permissions-reference.html
[aws-credentials]: https://docs.aws.amazon.com/sdk-for-java/v1/developer-guide/credentials.html
[kip-158]: https://cwiki.apache.org/confluence/display/KAFKA/KIP-158%3A+Kafka+Connect+should+allow+source+connectors+to+set+topic-specific+settings+for+new+topics


### Configuration


#### `streams.arn`

DynamoDB Stream ARN (Amazon Resource Name) of stream to consume by this connector. You
can find currently active table stream's ARN in AWS DynamoDB Console in table overview
or fetch it using AWS CLI or SDK. Note that when you disable and re-enable stream on
table, a new stream with new ARN will be created. If you want to consume it, you will
need to either re-configure this connector or provision a new one.


#### `kafka.topic`

Name of Kafka sink topic, to which captured records will be written.


#### `streams.initial.position`

Specifies position in the stream to reset to if no offsets are stored.
One of: `TRIM_HORIZON` (default) or `LATEST` (see _ShardIteratorType_ in [GetShardIterator]
[get-shard-iterator-request-docs].

[get-shard-iterator-request-docs]: https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_streams_GetShardIterator.html#API_streams_GetShardIterator_RequestSyntax


##### Warning note on LATEST
     
When you intend to use `LATEST`, you need to be aware of the following quirks:

* When you start a connector without any state built up yet, and task is stopped before
  records from all initial leaf shards are consumed, on the next start some data will
  be consumed from `TRIM_HORIZON`. If either no records were consumed or records from
  all shards were consumed and committed, you will not experience this.
* When you start a connector without any state built up yet, and no records are added
  to DynamoDB Stream, position in shard is not captured. If you then pause task for more
  than 10 minutes or stop it and records are added before task is resumed/started, these
  records will not be captured as the task will again start from *LATEST*, which points
  now to current stream's head.

If you have streams with infrequent events, what increases likeliness of experiencing
above quirks, you will be better off with leaving `streams.initial.position` at its
default `TRIM_HORIZON`.


#### `streams.get.records.min.interval.ms`

Defines minimum interval between consecutive GetRecords calls per shard, effectively
driving rate at which calls are made. For example, when default value 1000 is used,
the connector will not make GetRecords calls more often than once every second for
concurrently consumed shard. When 2 shards are processed concurrently, the connector
will make at most 2 such calls per second - one for each shard.
                                          
When choosing this value, you are making a trade-off between cost and latency. Higher
value will result in less calls and therefore less costs (see DynamoDB Streams pricing)
but will impact latency between making change in DynamoDB and seeing it in Kafka. Too
high value may cause lagging behind, as single call returns at most 1000 records or
1MB, whichever is hit first.
                                          
The value specified here is a minimum interval. The effective interval depends on
number of concurrently processed shards and DynamoDB Streams API response time.
The default is 1000 (1 second).


Alternatives
------------

### Kinesis Client Library

You can consume DynamoDB Streams by writing an application based on [Kinesis Client
Library][kcl] (KCL) with [DynamoDB Streams Kinesis Adapter][dynamodb-adapter]. If you
are already using Kafka, using the connector instead of processing records directly
from stream with KCL has the following advantages:

* Number of shards/partitions defines how many parallel consumer instances you can use
  to process captured changes in order and therefore drives your maximum throughput.
  In DynamoDB, number of shards is somewhat tied to number of DynamoDB table partitions
  and you cannot configure it. You can however decide how many partitions will your
  Kafka topic have and therefore control maximum number of parallel consumers.
* You gain option to have longer retention. DynamoDB Streams has fixed retention of 24
  hours. In Kafka you can configure topic's retention to your needs.
* DynamoDB Streams allows at most 2 concurrent consumer groups per stream. You don't
  have such limits in Kafka.
* It is easy to provision: When you already use Kafka Connect, adding another DynamoDB
  table is a matter granting needed IAM permissions and making an HTTP call.
* It is easy to operate: The task is running on your Kafka Connect cluster you already
  maintain. If your Kafka Connect cluster has multiple nodes, you also gain high
  availability, as Kafka Connect will take care of re-scheduling task to another
  node in case node crashes. Connector state is persisted in Kafka, so you don't need
  to keep a DynamoDB table per stream consumer as in case of KCL.
* You can [keep your data pipelines unified][kreps-unified].

[kcl]: https://github.com/awslabs/amazon-kinesis-client
[dynamodb-adapter]: https://github.com/awslabs/dynamodb-streams-kinesis-adapter
[kreps-unified]: https://engineering.linkedin.com/distributed-systems/log-what-every-software-engineer-should-know-about-real-time-datas-unifying


### Trustpilot's Kafka Connect Source Connector for DynamoDB

Trustpilot has open-sourced their DynamoDB Source Connector in June 2019 (
[kafka-connect-dynamodb][trustpilot-connector]). However, since we haven't
found it when looking in September 2019, we have decided to develop our own
connector. Both connectors aim to achieve the same task, but they vary in
approach and functionality offered:

* Trustpilot's connector is using KCL with adapter, whereas this connector
  communicates with DynamoDB Streams API directly. We chose to not use KCL,
  as:
   * KCL keeps its state in DynamoDB, creating a table for each stream it
     consumes. This connector keeps all the state in Kafka Connect and does
     not require any additional state storage.
   * We had negative experience working with KCL and DynamoDB Streams Adapter.
     We ran into some operational issues, we have also reported some bugs and
     analyzed root causes, but maintainers did not react much ([example 1]
     [issue-211], [example 2][issue-21]).
   * After analyzing adapter's code, we have concluded that it is not easy
     to adapt to Kafka Connect. The project does not have sufficient code
     quality - looks like a workaround to make KCL work with DynamoDB, even
     though KCL was built for Kinesis Streams which is modelled differently.
     Using it for this project would make it hard to analyze issues. 
* Trustpilot's connector has more features:
   * It auto-discovers tables to connect using AWS tags, so you don't need
     to configure connector for each table. Our connector requires you to
     define a connector for each table separately. We may add auto-discovery
     to this project when we see demand for it.
   * It can snapshot/init-sync table, that is put all existing table records to
     Kafka prior to streaming changes. This way you can replicate table
     entirely from Kafka, not missing anything. Our connector does not provide
     such functionality - we put in Kafka changes only. We may however add such
     feature when we see demand for it.

[trustpilot-connector]: https://github.com/trustpilot/kafka-connect-dynamodb
[issue-211]: https://github.com/awslabs/amazon-kinesis-client/issues/211
[issue-21]: https://github.com/awslabs/dynamodb-streams-kinesis-adapter/issues/21


Limitations
-----------

### Scalability

We support only one task per stream, where all concurrent shards are polled in
round robin fashion. The implementation shouldn't have any problems with hundreds
of records per second spread across several concurrent shards. However, if you have
tens of concurrent shards or thousands of records per second, you need to test if 
offered performance is sufficient. If you have hundreds or thousands of concurrent
shards, the implementation needs to be changed or you may need to look for another
solution.

We decided to use only one task per stream because:

* It was suitable for our requirements.
* Kafka Connect doesn't seem to support cases like DynamoDB Streams well.

What is special about DynamoDB Streams? Two things:

* To know which shards are ready to be consumed (following lineage of shards),
  you need to know whether their parents were already consumed. Whether parents
  were consumed is something that only consuming task knows. However, Kafka
  Connect doesn't provide any mechanisms to communicate between tasks and
  connector to coordinate work.
* DynamoDB Streams shards are pretty dynamic - shards are replaced (closed and
  superseded by their children) every several hours. In Kafka Connect view,
  partitions are however rather static. For example, Kafka Connect stores offsets
  per partition in a log compacted topic. If you would use shards as partitions,
  you would need to add also a clean-up process, which would remove offsets for
  old shards.

That being said, it may be possible to partition DynamoDB stream. It requires
a bit of thinking and may need some workarounds to get going, but it seems
possible.


### One connector per stream 

One connector captures changes from only one stream. If you want to capture 
changes from more tables, configure connector for each stream separately.
This could be changed if there is a good case for it.


Operations
----------

### Resiliency

The connector will deal with transient problems including:

* connectivity issues (network problems)
* service unavailability (DynamoDB Streams returning 5XX errors)
* throttling

The connector will keep re-trying requests with exponential back-off until
it succeeds.


### State management

All state is written to Kafka Connect's offsets topic. We keep single state per
stream (stream is our sole partition), from which we automatically clean up no
longer needed offsets for older shards.

There is no point in backing up the state, since if you lose your Kafka cluster,
you should anyways start from `TRIM_HORIZON`.


### Monitoring

Kafka Connect doesn't support reporting connector-specific metrics, nor it
exposes access to connector name and task ID, which would be helpful to tag
metrics reported in any other way. Therefore, lag on stream is currently not
exposed. Nevertheless, [Kafka Connect exposes useful metrics][kip-196], that you
can monitor.

[kip-196]: https://cwiki.apache.org/confluence/display/KAFKA/KIP-196%3A+Add+metrics+to+Kafka+Connect+framework


### Disabling stream

When you disable a stream, the connector will continue to work. Once all records
are consumed, it will no longer make _GetShardIterator_ nor _GetRecords_ requests,
but it will keep calling _DescribeStream_. The implementation could be improved,
but ideally you just stop the connectors you no longer need.

If you disable and re-enable stream on a DynamoDB table, a new stream will be
created. An existing connector will not start consuming it. You need to
re-configure the connector for it to consume the new stream. Note, that you will
lose changes that happened between disabling and re-enabling the table stream.


Development
-----------

The following software is required to work with the project locally:

* Java Development Kit 8 (or newer) (for example [OpenJDK 8][open-jdk])
* [Docker Engine][docker-engine]

[open-jdk]: http://openjdk.java.net/projects/jdk8/
[docker-engine]: http://docs.docker.com/engine/installation/

To build the project locally and run unit tests run:

```./gradlew test```

To run integration tests run (requires Docker installed):

```./gradlew integrationTest```

To prepare archive for deployment run:

```./gradlew packageDistribution```

The archive will be put in `build/dist` directory.


Contributing
------------

Please read [CONTRIBUTING.md](CONTRIBUTING.md).


License
-------

Copyright 2020 Nexxiot AG and contributors. Licensed under the Apache License 2.0,
see the [LICENSE.txt](LICENSE.txt) file for details.

Third party components and dependencies are covered by the following licenses
- see the [LICENSE-3rd-PARTIES.txt](LICENSE-3rd-PARTIES.txt) file for details:

* Apache Software License version 2.0
   * [Kotlin](https://github.com/JetBrains/kotlin)
   * [AWS SDK for Java](https://github.com/aws/aws-sdk-java)
   * [Guava](https://github.com/google/guava)
