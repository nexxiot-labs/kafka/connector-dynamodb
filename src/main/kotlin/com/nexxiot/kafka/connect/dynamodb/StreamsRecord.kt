package com.nexxiot.kafka.connect.dynamodb

import com.amazonaws.services.dynamodbv2.model.AttributeValue

class StreamsRecord(
    val sourcePartition: Map<String, String>,
    val sourceOffset: Map<String, String>,
    val key: Map<String, AttributeValue>,
    val event: DynamoDbChangeEvent
)