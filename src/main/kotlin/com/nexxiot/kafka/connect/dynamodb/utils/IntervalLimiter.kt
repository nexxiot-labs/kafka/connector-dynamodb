package com.nexxiot.kafka.connect.dynamodb.utils

import com.google.common.base.Stopwatch
import com.google.common.util.concurrent.Uninterruptibles
import java.time.Duration

/**
 * An interval limiter, allowing to enforce, that code is not executed more
 * often than [minInterval]. Each [waitForNext] blocks if necessary until
 * a next code execution is allowed.
 *
 * We chose to not use Guava's [com.google.common.util.concurrent.RateLimiter],
 * as its bursty limiter does provide what we need (we don't want bursts).
 *
 * This class is not thread-safe.
 */
@Suppress("UnstableApiUsage")
class IntervalLimiter(
    private val minInterval: Duration
) {
    private val stopwatch: Stopwatch = Stopwatch.createStarted()
    private var nextTicket: Duration = stopwatch.elapsed()

    /**
     * Blocks until next code execution adhering to minimum interval that
     * is allowed.
     */
    fun waitForNext() {
        val now = stopwatch.elapsed()
        val waitTime = timeUntilNext(now)
        if (waitTime > Duration.ZERO) {
            Uninterruptibles.sleepUninterruptibly(waitTime)
        }
        nextTicket = now.plus(waitTime).plus(minInterval)
    }

    /**
     * Returns time needed to wait until next execution is possible.
     * If no wait is needed, [Duration.ZERO] is returned.
     */
    fun timeUntilNext(): Duration {
        return timeUntilNext(stopwatch.elapsed())
    }

    /**
     * Returns time needed to wait until next execution is possible.
     * If no wait is needed, [Duration.ZERO] is returned.
     */
    private fun timeUntilNext(now: Duration): Duration {
        // if nextTicket is in the past, align it to now, so we end up with 0 wait time instead of negative
        var next = this.nextTicket
        if (now > next) {
            next = now
        }
        return next.minus(now)
    }
}