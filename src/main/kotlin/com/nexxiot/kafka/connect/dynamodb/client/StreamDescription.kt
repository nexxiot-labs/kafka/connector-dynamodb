package com.nexxiot.kafka.connect.dynamodb.client

data class StreamDescription(
    val arn: String,
    val tableName: String,
    val status: String,
    val viewType: String,
    val keySchema: KeySchema,
    val shards: List<Shard>
) {
    val lastEvaluatedShardId: String?
        get() = shards.lastOrNull()?.id

    data class KeySchema(
        val partitionKeyAttrName: String
    )
}