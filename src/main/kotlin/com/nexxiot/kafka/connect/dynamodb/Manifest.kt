package com.nexxiot.kafka.connect.dynamodb

object Manifest {

    fun getVersion(): String {
        return this::class.java.getPackage().implementationVersion
    }
}