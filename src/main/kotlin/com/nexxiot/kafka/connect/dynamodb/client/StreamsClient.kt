package com.nexxiot.kafka.connect.dynamodb.client

import com.amazonaws.AmazonClientException
import com.amazonaws.AmazonWebServiceResult
import com.amazonaws.ResponseMetadata
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBStreams
import com.amazonaws.services.dynamodbv2.model.DescribeStreamRequest
import com.amazonaws.services.dynamodbv2.model.GetRecordsRequest
import com.amazonaws.services.dynamodbv2.model.GetRecordsResult
import com.amazonaws.services.dynamodbv2.model.GetShardIteratorRequest
import com.amazonaws.services.dynamodbv2.model.GetShardIteratorResult
import com.amazonaws.services.dynamodbv2.model.KeyType
import com.amazonaws.services.dynamodbv2.model.StreamStatus
import com.google.common.util.concurrent.RateLimiter
import java.time.Duration
import java.time.Instant
import java.util.LinkedList

/**
 * Wraps Amazon's client. Hides some details like pagination and adds
 * retryable error handling. The error handling, in addition to SDK's default
 * handling, maintains exponential back-off across multiple calls, giving
 * control back to caller if wait is needed before the next try. This allows
 * for longer back-offs without blocking thread for too long.
 */
class StreamsClient(
    private val awsStreamsClient: AmazonDynamoDBStreams
) {
    // DynamoDB DescribeStreams allows up to 10 requests per second
    // Limit requests to pro-actively prevent throttling
    @Suppress("UnstableApiUsage")
    private val describeStreamRateLimiter: RateLimiter = RateLimiter.create(8.0)

    private val retryPolicy = RetryPolicy(
        Duration.ofMillis(250),
        Duration.ofSeconds(15)
    )

    /**
     * Number of consecutive, failed, retryable errors when calling AWS SDK,
     * keyed by operation type.
     */
    private val failedAttempts = HashMap<String, Int>()

    /**
     * Time, until which retry back-off is effective. To respect back-off,
     * re-try cannot be attempted before the deadline is reached.
     */
    private val backoffDeadlines = HashMap<String, Instant>()

    /**
     * Returns [StreamDescription] with all currently available shards (paginating
     * results if necessary).
     *
     * The given stream must be in [StreamStatus.ENABLED] state or further, as
     * otherwise shards are not necessarily yet initialized. The method throws
     * [IllegalArgumentException] if stream is in [StreamStatus.ENABLING] state.
     */
    fun describeStreamWithAllShards(streamArn: String): StreamDescription {
        describeStreamRateLimiter.acquire()
        val result = withRetriesHandling("DescribeStream") {
            describeStreamRateLimiter.acquire()
            val request = DescribeStreamRequest().withStreamArn(streamArn)
            awsStreamsClient.describeStream(request)
        }
        val streamDesc = result.streamDescription

        require(streamDesc.streamStatus != StreamStatus.ENABLING.toString()) {
            "Stream $streamArn is still in ${streamDesc.streamStatus} state. Shards may not be initialized yet."
        }

        // Get shards
        val shards = LinkedList<Shard>()
        streamDesc.shards.mapTo(shards, Companion::mapShard)
        streamDesc.lastEvaluatedShardId?.let { lastEvaluatedShardId ->
            shards.addAll(getAllShards(streamArn, lastEvaluatedShardId))
        }

        val keySchema =
            StreamDescription.KeySchema(
                streamDesc.keySchema.single { attr ->
                    KeyType.fromValue(attr.keyType) == KeyType.HASH
                }.attributeName
            )

        return StreamDescription(
            streamDesc.streamArn,
            streamDesc.tableName,
            streamDesc.streamStatus,
            streamDesc.streamViewType,
            keySchema,
            shards
        )
    }

    /**
     * Fetches page of stream's shards starting from [lastEvaluatedShardId]
     * (exclusive), or first page if [lastEvaluatedShardId] is null.
     */
    fun getShards(streamArn: String, lastEvaluatedShardId: String? = null): GetShardsResult {
        val result = withRetriesHandling("DescribeStream") {
            describeStreamRateLimiter.acquire()
            val request = DescribeStreamRequest()
                .withStreamArn(streamArn)
                .withExclusiveStartShardId(lastEvaluatedShardId)
            awsStreamsClient.describeStream(request)
        }
        val streamDesc = result.streamDescription
        val shards = streamDesc.shards.map(Companion::mapShard)
        return GetShardsResult(shards, streamDesc.lastEvaluatedShardId != null)
    }

    fun getShardIterator(request: GetShardIteratorRequest): GetShardIteratorResult {
        return withRetriesHandling("GetShardIterator") {
            awsStreamsClient.getShardIterator(request)
        }
    }

    fun getRecords(request: GetRecordsRequest): GetRecordsResult {
        return withRetriesHandling("GetRecords") {
            awsStreamsClient.getRecords(request)
        }
    }

    private fun <T : AmazonWebServiceResult<ResponseMetadata>> withRetriesHandling(
        operation: String,
        requestFn: () -> T
    ): T {
        return try {
            val deadline = backoffDeadlines[operation]
            if (deadline != null) {
                val remainingBackoff = Duration.between(Instant.now(), deadline)
                if (remainingBackoff > Duration.ZERO) {
                    // Hand-off control to caller, as there is still some wait involved
                    throw RetryableException(
                        operation,
                        null,
                        remainingBackoff
                    )
                }
            }

            val result = requestFn()
            failedAttempts[operation] = 0
            backoffDeadlines.remove(operation)
            result
        } catch (ex: AmazonClientException) {
            if (retryPolicy.isRetryable(ex)) {
                val failedAttemptsCount = 1 + failedAttempts.getOrDefault(operation, 0)
                failedAttempts[operation] = failedAttemptsCount
                val retriesAttempted = failedAttemptsCount - 1
                val backoffTime = retryPolicy.delayBeforeRetry(retriesAttempted)
                val backoffDeadline = Instant.now().plus(backoffTime)
                backoffDeadlines[operation] = backoffDeadline
                throw RetryableException(
                    operation,
                    ex,
                    backoffTime
                )
            } else throw ex
        }
    }

    /**
     * Paginates through DescribeStream to fetch all stream's shards starting
     * from [lastEvaluatedShardId] (exclusive), or first page if [lastEvaluatedShardId]
     * is null. The method returns only once there are no more shards to get.
     */
    private fun getAllShards(streamArn: String, lastEvaluatedShardId: String? = null): List<Shard> {
        val foundShards = LinkedList<Shard>()

        var lastShardId: String? = lastEvaluatedShardId
        do {
            val getShardsResult = getShards(streamArn, lastShardId)
            foundShards.addAll(getShardsResult.shards)
            lastShardId = getShardsResult.lastEvaluatedShardId
        } while (getShardsResult.hasNextPage)

        return foundShards
    }

    companion object {
        private fun mapShard(shard: com.amazonaws.services.dynamodbv2.model.Shard): Shard {
            return Shard(
                shard.shardId,
                shard.parentShardId,
                shard.sequenceNumberRange.endingSequenceNumber
            )
        }
    }
}