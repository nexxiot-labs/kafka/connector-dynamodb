package com.nexxiot.kafka.connect.dynamodb.client

import com.nexxiot.kafka.connect.dynamodb.SequenceNumber
import com.nexxiot.kafka.connect.dynamodb.ShardId

data class Shard(

    val id: ShardId,

    /**
     * ID of shard, which is parent to this shard. Having a child shard means
     * the parent shard has been closed. To make sure records are consumed
     * in order, consume all records in parent shard before consuming records
     * from this shard (follow lineage of shards).
     */
    val parentId: ShardId?,

    /**
     * Shard's ending sequence number. Available only when shard was already closed
     * when we discovered it during DescribeStream operation. Because we discover
     * new shards incrementally, we may have shards which are currently closed,
     * but for which we don't have ending sequence number.
     */
    val endingSeqNumber: SequenceNumber?
)