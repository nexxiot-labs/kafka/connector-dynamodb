package com.nexxiot.kafka.connect.dynamodb

class DynamoDbSourceTaskConfig(
    props: Map<String, String>
) : DynamoDbSourceConnectorConfig(props)