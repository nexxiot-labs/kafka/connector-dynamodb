package com.nexxiot.kafka.connect.dynamodb

import com.amazonaws.services.dynamodbv2.document.ItemUtils
import com.amazonaws.services.dynamodbv2.model.AttributeValue
import com.nexxiot.kafka.connect.data.JavaInstant
import com.nexxiot.kafka.connect.data.Json
import org.apache.kafka.connect.data.SchemaBuilder
import org.apache.kafka.connect.data.Struct
import java.time.Instant

@Suppress("MemberVisibilityCanBePrivate")
class DynamoDbChangeEvent(
    /** A globally unique identifier for the event that was recorded in DynamoDB Stream, assigned by AWS. */
    val id: String,
    /** See [com.amazonaws.services.dynamodbv2.model.Record.getEventName] */
    val type: String,
    /** Approximate date and time when the stream record was created (AWS calls it approximate). */
    val approximateTime: Instant,
    val keys: Map<String, AttributeValue>,
    val newImage: Map<String, AttributeValue>?,
    val oldImage: Map<String, AttributeValue>?,
    val discoverTime: Instant
) {
    fun toStruct(): Struct {
        val struct = Struct(SCHEMA)
            .put(Fields.ID, id)
            .put(Fields.TYPE, type)
            .put(Fields.APPROX_TIME, approximateTime.toString())
            .put(Fields.KEYS, ItemUtils.toItem(keys).toJSON())
            .put(Fields.DISCOVER_TIME, discoverTime.toString())

        if (!newImage.isNullOrEmpty()) {
            struct.put(Fields.NEW_IMAGE, ItemUtils.toItem(newImage).toJSON())
        }

        if (!oldImage.isNullOrEmpty()) {
            struct.put(Fields.OLD_IMAGE, ItemUtils.toItem(oldImage).toJSON())
        }

        return struct
    }

    companion object {
        val SCHEMA: SchemaBuilder = SchemaBuilder.struct()
            .name("com.nexxiot.dynamodb.streams.ChangeEvent")
            .doc(
                "Data mapped from DynamoDB model's Record (see " +
                    "https://docs.aws.amazon.com/amazondynamodb/latest/APIReference/API_streams_Record.html)"
            )
            .version(1)
            .field(Fields.ID, SchemaBuilder.string())
            .field(
                Fields.APPROX_TIME,
                JavaInstant.builder()
                    .doc("Approximate time when the stream record was created.")
                    .schema()
            )
            .field(Fields.TYPE, SchemaBuilder.string())
            .field(Fields.KEYS, Json.SCHEMA)
            .field(Fields.NEW_IMAGE, Json.builder().optional().schema())
            .field(Fields.OLD_IMAGE, Json.builder().optional().schema())
            .field(
                Fields.DISCOVER_TIME, JavaInstant.builder()
                    .doc("Time, when record was captured by Kafka Connect DynamoDBSourceConnector.")
                    .schema()
            )
    }

    object Fields {
        const val ID = "id"
        const val APPROX_TIME = "approx_time"
        const val TYPE = "type"
        const val KEYS = "keys"
        const val NEW_IMAGE = "new_image"
        const val OLD_IMAGE = "old_image"
        const val DISCOVER_TIME = "discover_time"
    }
}
