package com.nexxiot.kafka.connect.dynamodb

import com.amazonaws.ClientConfiguration
import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.client.builder.AwsClientBuilder
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBStreams
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBStreamsClientBuilder
import com.amazonaws.services.dynamodbv2.model.AttributeValue
import com.nexxiot.kafka.connect.dynamodb.client.RetryableException
import com.nexxiot.kafka.connect.dynamodb.client.StreamDescription
import com.nexxiot.kafka.connect.dynamodb.client.StreamsClient
import org.apache.kafka.connect.data.Schema
import org.apache.kafka.connect.source.SourceRecord
import org.apache.kafka.connect.source.SourceTask
import org.slf4j.LoggerFactory
import org.slf4j.MDC
import java.net.ConnectException
import java.time.Duration
import java.util.concurrent.atomic.AtomicBoolean
import kotlin.concurrent.thread

@Suppress("unused")
class DynamoDbSourceTask : SourceTask {

    private val dependenciesInjected: Boolean
    private lateinit var awsStreamsClient: AmazonDynamoDBStreams

    private lateinit var config: DynamoDbSourceTaskConfig
    private lateinit var streamConsumer: StreamShardsConsumer
    private lateinit var shardScanner: StreamShardsScanner
    private lateinit var shardScannerThread: Thread

    /**
     * We use string, because:
     * - Attributes used for keys can be only of a scalar type, which is
     *   either a string, a number (which casting to long or double could
     *   truncate value) or binary (which we don't support at the moment).
     * - DescribeStream does not reveal types of attributes used for keys
     *   (you can fetch it by describing table though).
     * - Even if there is sort key, we ignore it, as it does not map well
     *   to Kafka keys (perhaps we change it if we find a good case for it).
     */
    private val keySchema: Schema = Schema.STRING_SCHEMA

    /**
     * Note that we can't provide schema for [DynamoDbChangeEvent.newImage]
     * and [DynamoDbChangeEvent.oldImage] unless it would be changing from
     * record to record (DynamoDB is schema on read, so we have no clue what
     * the schema is - except for keys, each record may have its own format).
     */
    private val valueSchema: Schema = DynamoDbChangeEvent.SCHEMA

    /**
     * Name of attribute, which serves as DynamoDB table's partition key.
     */
    private lateinit var partitionKeyAttrName: String

    private val shutdown: AtomicBoolean = AtomicBoolean(false)

    constructor() {
        dependenciesInjected = false
    }

    constructor(streamsClient: AmazonDynamoDBStreams) : super() {
        dependenciesInjected = true
        this.awsStreamsClient = streamsClient
    }

    override fun start(props: Map<String, String>) {
        val config =
            DynamoDbSourceTaskConfig(props) // TODO Did it validate already?
        this.config = config

        if (!dependenciesInjected) {
            awsStreamsClient = if (config.mockEndpointUrl != null) {
                log.info("Client: Using mock DynamoDB Streams with endpoint ${config.mockEndpointUrl}")
                getMockEndpointAwsStreamsClient(config.mockEndpointUrl)
            } else getDefaultAwsStreamsClient()
        }

        log.debug("Stream: Describing stream {}", config.streamArn)
        val streamsClient = StreamsClient(awsStreamsClient)
        val stream = streamsClient.describeStream(config.streamArn)
        log.info(
            "Stream: Stream for table {} has {} view type and is in {} state",
            stream.tableName,
            stream.viewType,
            stream.status
        )

        log.info("Shards: Detected {} shards", stream.shards.size)

        partitionKeyAttrName = stream.keySchema.partitionKeyAttrName

        streamConsumer = StreamShardsConsumer(
            streamsClient,
            config.streamArn,
            stream.shards,
            context.offsetStorageReader(),
            config.initialIteratorPosition,
            config.minGetRecordsInterval
        )

        shardScanner = StreamShardsScanner(
            streamsClient,
            config.streamArn,
            stream.lastEvaluatedShardId
        )
        val taskContext = MDC.getCopyOfContextMap()
        shardScannerThread = thread(isDaemon = true, name = "${Thread.currentThread().name}-ShardScanner") {
            MDC.setContextMap(taskContext)
            // Delay first execution - we have just pulled all shards
            Thread.sleep(Duration.ofSeconds(1).toMillis())
            shardScanner.captureNewShardsPeriodically()
        }

        // TODO Metrics. Perhaps could extract connector name and task id from thread context
    }

    override fun poll(): List<SourceRecord>? {
        val newShards = shardScanner.pollCapturedShards()
        streamConsumer.addNewShards(newShards)

        val pollResult = streamConsumer.pollReadyShardRoundRobin() ?: return null

        if (pollResult.reachedShardEnd) {
            shardScanner.notifyShardEndReached()
        }

        return if (pollResult.records.isNotEmpty()) {
            pollResult.records.map { record ->
                val partitionKeyValue: AttributeValue = record.key.getValue(partitionKeyAttrName)
                val kafkaPartitionKey = partitionKeyValue.s
                    ?: partitionKeyValue.n
                    ?: throw IllegalArgumentException("Tables with binary partition keys are currently not supported")

                SourceRecord(
                    record.sourcePartition,
                    record.sourceOffset,
                    config.kafkaTopic,
                    null, // Leave it to key partitioner
                    keySchema,
                    kafkaPartitionKey,
                    valueSchema,
                    record.event.toStruct(),
                    record.event.approximateTime.toEpochMilli()
                )
            }
        } else null // Kafka Connect rather expects null than empty collection
    }

    override fun stop() {
        shutdown.set(true)

        if (::shardScanner.isInitialized) { // Could be not initialized if failed during start
            shardScanner.shutdown()
        }
        // Nothing to do with poll - it shouldn't take more than few seconds for poll to return.
    }

    override fun version(): String = Manifest.getVersion()

    private fun StreamsClient.describeStream(streamArn: String): StreamDescription {
        while (!shutdown.get()) {
            try {
                return describeStreamWithAllShards(streamArn)
            } catch (ex: IllegalArgumentException) {
                log.info("RetryableException: Stream is in ENABLING state, which may result in inconsistent " +
                    "results. Will re-try soon..")
                Thread.sleep(1000)
            } catch (ex: RetryableException) {
                if (!ex.returnedControlWithoutTrying) {
                    log.warn("RetryableException: ${ex.message}", ex)
                }
                Thread.sleep(250)
            }
        }
        throw ConnectException("Task stopped before managed to describe stream")
    }

    companion object {
        private val log = LoggerFactory.getLogger(DynamoDbSourceTask::class.java)

        private fun getDefaultAwsStreamsClient(): AmazonDynamoDBStreams {
            //
            // Tuning client, so that it gives control back earlier:
            // - decreasing retries made by SDK from default 3 to 2
            // - decreasing socket timeout
            //
            return AmazonDynamoDBStreamsClientBuilder.standard()
                .withClientConfiguration(
                    ClientConfiguration()
                        .withMaxErrorRetry(2)
                        .withSocketTimeout(Duration.ofSeconds(5).toMillis().toInt())
                        .withCacheResponseMetadata(false)
                )
                .build()
        }

        /**
         * Prepares a client to work with mock, local DynamoDB Streams instance.
         */
        private fun getMockEndpointAwsStreamsClient(mockEndpointUrl: String): AmazonDynamoDBStreams {
            return AmazonDynamoDBStreamsClientBuilder.standard()
                .withEndpointConfiguration(
                    AwsClientBuilder.EndpointConfiguration(mockEndpointUrl, "us-east-1")
                )
                .withCredentials(
                    AWSStaticCredentialsProvider(BasicAWSCredentials("dummy-key", "dummy-secret"))
                )
                .build()
        }
    }
}
