package com.nexxiot.kafka.connect.dynamodb

typealias ShardId = String
typealias ShardIterator = String
typealias SequenceNumber = String