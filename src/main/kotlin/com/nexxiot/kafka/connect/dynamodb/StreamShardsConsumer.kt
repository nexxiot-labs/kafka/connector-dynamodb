package com.nexxiot.kafka.connect.dynamodb

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBStreams
import com.amazonaws.services.dynamodbv2.model.ExpiredIteratorException
import com.amazonaws.services.dynamodbv2.model.GetRecordsRequest
import com.amazonaws.services.dynamodbv2.model.GetRecordsResult
import com.amazonaws.services.dynamodbv2.model.GetShardIteratorRequest
import com.amazonaws.services.dynamodbv2.model.ShardIteratorType
import com.amazonaws.services.dynamodbv2.model.TrimmedDataAccessException
import com.google.common.graph.GraphBuilder
import com.google.common.graph.MutableGraph
import com.google.common.util.concurrent.Uninterruptibles
import com.nexxiot.kafka.connect.dynamodb.client.RetryableException
import com.nexxiot.kafka.connect.dynamodb.client.Shard
import com.nexxiot.kafka.connect.dynamodb.client.StreamsClient
import com.nexxiot.kafka.connect.dynamodb.utils.IntervalLimiter
import org.apache.kafka.connect.storage.OffsetStorageReader
import org.slf4j.LoggerFactory
import java.time.Duration
import java.time.Instant
import java.util.LinkedList
import java.util.Queue

/**
 * Takes care of consuming DynamoDB Streams' stream shards following lineage
 * of shards, which guarantees processing records in order (order is guaranteed
 * per key). The consumer does not however monitor stream for new shards - these
 * have to be provided from outside.
 *
 * It is meant to work DynamoDB Streams shards, and as such it works with
 * following assumptions:
 *
 * - Shards form a Directed Acyclic Graph (DAG) with potentially multiple roots.
 * - Shard can have at most one parent - shards can be split, but never merged.
 * - It is okay for shard to list parent, which does not exist (assuming the
 *   parent was trimmed). In such case, the shard will be considered a root.
 * - Shards are open for writing for no more than 24 hours. That is there are
 *   no shards that are open and which data was already trimmed. Trimming data
 *   occurs only after shard was closed.
 *
 * Upon initialization, an exhaustive, consistent list of all shards must be
 * given or otherwise unexpected behavior may occur. You can get exhaustive,
 * consistent list of all shards by fetching all pages of DescribeStream for
 * a stream that is in ENABLED, DISABLING or DISABLED state (not ENABLING!).
 *
 * The class is not thread safe.
 *
 * @param shards Exhaustive list of all available shards. The shards must meet
 *        assumptions made for DynamoDB shards (see above). The list must
 *        include all shards returned for stream in consistent state
 *        (once it completes ENABLING).
 * @param offsetStorageReader Required to restore previous consumption state.
 */
class StreamShardsConsumer(
    private val streamsClient: StreamsClient,
    private val streamArn: String,
    shards: Collection<Shard>,
    offsetStorageReader: OffsetStorageReader,
    initialIteratorPosition: ShardIteratorType,
    private val minGetRecordsInterval: Duration
) {
    /**
     * Kafka Connect's partition of source system.
     * We consider stream to be a partition.
     */
    private val sourcePartition = mapOf("stream_arn" to streamArn)

    /**
     * A Directed Acyclic Graph capturing relations between shards.
     * Shard's children are its successors.
     */
    @Suppress("UnstableApiUsage")
    private val graph: MutableGraph<ShardId>

    /**
     * IDs of shards in graph, which have no parent - used as starting
     * point when traversing graph in shards' lineage.
     */
    private val rootShardIds: HashSet<ShardId>

    /**
     * Shard's initial position to use for root shards if they don't have
     * any existing iterator or watermark.
     */
    private val rootShardsInitialPosition: ShardIteratorType

    /**
     * Iterators to be used to fetch next shard's records. Note that an
     * iterator expires after 15 minutes, in which case iterator will need
     * to be obtained by sequence number using [AmazonDynamoDBStreams.getShardIterator].
     */
    private val shardIterators = HashMap<ShardId, ShardIterator>()

    /**
     * A shard's watermark is sequence number of the last consumed
     * record in shard.
     */
    private val shardWatermarks = HashMap<ShardId, SequenceNumber>()

    /**
     * IDs of shards, which were fully consumed, that is we have processed
     * all their records.
     */
    private val completedShardIds = HashSet<ShardId>()

    /**
     * IDs of shards, which are ready for consumption following shards'
     * lineage. It has to be updated, whenever new shards are completed.
     * When new shards are detected, it is updated by re-instantiating
     * this class with new task configuration.
     */
    private var readyShardIds: List<ShardId> = emptyList()
    private var roundRobinShardIndex = -1

    /**
     * Interval limiters pace [AmazonDynamoDBStreams.getRecords] calls,
     * allowing to control DynamoDB Streams costs. Increasing rate results
     * in lower costs, but higher latency.
     */
    private val shardIntervalLimiters = HashMap<ShardId, IntervalLimiter>()

    init {
        require(setOf(ShardIteratorType.TRIM_HORIZON, ShardIteratorType.LATEST).contains(initialIteratorPosition)) {
            "Only ${ShardIteratorType.TRIM_HORIZON} and ${ShardIteratorType.LATEST} " +
                "initial iterator positions are supported"
        }

        val givenShardIds = shards.map { it.id }.toSet()
        this.rootShardIds = shards.asSequence()
            .filter { it.parentId == null || !givenShardIds.contains(it.parentId) }
            .map { it.id }
            .toHashSet()

        // Build directed shards graph to track lineage
        @Suppress("UnstableApiUsage")
        this.graph = GraphBuilder.directed().build<ShardId>()
        shards.forEach {
            graph.addNode(it.id)
            if (it.parentId != null && givenShardIds.contains(it.parentId)) {
                graph.putEdge(it.parentId, it.id)
            }
        }

        val storedShardWatermarks = offsetStorageReader.offset(this.sourcePartition)
        if (storedShardWatermarks != null && storedShardWatermarks.isNotEmpty()) {
            // Restore persisted watermarks
            storedShardWatermarks
                .filter { (shardId, _) -> givenShardIds.contains(shardId) }
                .forEach { (shardId, watermark) ->
                    this.shardWatermarks[shardId] = watermark as SequenceNumber
                }

            // Mark already completed shards
            shards
                .filter { shard ->
                    // We consider shard completed, if:
                    // a. Shard has ending sequence number and it is equal to stored offset
                    // b. Shard does not have ending sequence, but we have offset stored for any of its descendants
                    //    (such inconsistency could occur when shard was closed during pagination)
                    val shardFullyConsumed = shard.endingSeqNumber != null
                        && this.shardWatermarks[shard.id] == shard.endingSeqNumber
                    return@filter shardFullyConsumed || hasConsumedDescendants(shard.id)
                }
                .mapTo(this.completedShardIds) { shard -> shard.id }
        }

        if (storedShardWatermarks.isNullOrEmpty() && initialIteratorPosition == ShardIteratorType.LATEST) {
            // There is no state, so we are just starting to consume the stream.
            // We want to consume from LATEST, so we need to ignore all non-leaf
            // shards. For leaf shards, we need to fetch iterator of type LATEST.
            val leafShards = givenShardIds
                .filter { shardId -> graph.successors(shardId).isEmpty() }
                .toSet()

            // Drop non-leaf shards
            this.rootShardIds.clear()
            this.rootShardIds.addAll(leafShards)
            givenShardIds
                .filterNot { leafShards.contains(it) }
                .forEach { this.graph.removeNode(it) }

            this.rootShardsInitialPosition = ShardIteratorType.LATEST
        } else {
            this.rootShardsInitialPosition = ShardIteratorType.TRIM_HORIZON
        }

        updateReadyShardIds()
    }

    /**
     * Adds shards to existing graph of shards. The new [shards] must be children
     * of already known shards or children of given [shards].
     *
     * Note: This class is not thread safe. Calls to polling and adding new shards
     * should happen from the same thread.
     */
    fun addNewShards(shards: Collection<Shard>) {
        if (shards.isEmpty()) {
            return
        }

        val knownShardIds = HashSet(graph.nodes())
        shards.mapTo(knownShardIds) { it.id }
        shards.forEach { shard ->
            require(shard.parentId == null || knownShardIds.contains(shard.parentId)) {
                "New shard ${shard.id} has unknown parent ${shard.parentId}"
            }
            graph.addNode(shard.id)
            shard.parentId?.let { parentId ->
                graph.putEdge(parentId, shard.id)
            }
        }

        updateReadyShardIds()
    }

    /**
     * Polls records from one of the ready-for-consumption shards. Shard to poll from
     * is selected in a round-robin fashion. A read-for-consumption shard is shard,
     * whose parents were fully consumed. This ensures shards are consumed in lineage,
     * guaranteeing returning records in order (within single key).
     */
    fun pollReadyShardRoundRobin(): PollResult? {
        if (readyShardIds.isEmpty()) {
            // There are no shards ready for consumption. There is nothing to do
            // until new shards are discovered.
            @Suppress("UnstableApiUsage")
            Uninterruptibles.sleepUninterruptibly(minGetRecordsInterval)
            return null
        }

        val shardIndex = (roundRobinShardIndex + 1) % readyShardIds.size
        roundRobinShardIndex = shardIndex
        val shardId = readyShardIds[shardIndex]

        val pollIntervalLimiter = shardIntervalLimiters.getOrPut(shardId, {
            IntervalLimiter(minGetRecordsInterval)
        })
        pollIntervalLimiter.waitForNext()

        val getRecordsResult = try {
            getRecords(shardId)
        } catch (ex: ExpiredIteratorException) {
            log.info(
                "ExpiredIterator: Iterator for shard {} expired. Fetching new shard iterator and re-trying",
                shardId
            )

            // Drop expired iterator
            shardIterators.remove(shardId)

            // Re-try consuming shard
            getRecords(shardId)
        } catch (ex: TrimmedDataAccessException) {
            log.warn(
                "TrimmedData: Data in shard {} was trimmed between fetching iterator and using it. " +
                    "Most probably some records were lost. Will continue processing from current TRIM_HORIZON",
                shardId
            )
            // Drop iterator in case it exists, didn't expire, but points to trimmed data
            shardIterators.remove(shardId)
            // Re-try consuming shard
            getRecords(shardId)

            //
            // If shards would be open for more than 24 hours, we could run into following
            // problem: if there are no activity on table for over 24 hours, we would keep
            // sequence number of trimmed record. When polling, this would result in
            // TrimmedDataAccessException. We would try to get new iterator, and we would
            // get iterator for TRIM_HORIZON. However, we would get no records and therefore
            // we would not update watermark. On next poll, we would get TrimmedData again.
            // Luckily, shards are open for up to 4 hours. It isn't well documented (as
            // DynamoDB Streams generally), but I found 2 mentions of it:
            // - https://forums.aws.amazon.com/thread.jspa?threadID=166153
            // - https://aws.amazon.com/blogs/big-data/process-large-dynamodb-streams-using-multiple-amazon-kinesis-client-library-kcl-workers/
            //
        } catch (ex: RetryableException) {
            if (!ex.returnedControlWithoutTrying) {
                log.warn("RetryableException: ${ex.message}", ex)
            }

            //
            // We should wait before giving control back to Kafka Connect to avoid "busy"
            // wait. However, when poll gets called again, we will wait a bit due to
            // shardIntervalLimiters, effectively eliminating busy wait.
            //
            return null
        }

        log.debug(
            "PollResult: Got {} records from shard {}. Sequence number of last record: {}",
            getRecordsResult.records.size,
            shardId,
            getRecordsResult.records.lastOrNull()?.dynamodb?.sequenceNumber ?: "-"
        )

        val discoverTime = Instant.now()
        val sourceRecords = getRecordsResult.records.map { record ->
            StreamsRecord(
                sourcePartition = sourcePartition,
                sourceOffset = getSourceOffsetForRecord(shardId, record.dynamodb.sequenceNumber),
                key = record.dynamodb.keys,
                event = DynamoDbChangeEvent(
                    record.eventID,
                    record.eventName,
                    record.dynamodb.approximateCreationDateTime.toInstant(),
                    record.dynamodb.keys,
                    record.dynamodb.newImage,
                    record.dynamodb.oldImage,
                    discoverTime
                )
            )
        }

        // Update cached processing state
        getRecordsResult.records.lastOrNull()?.dynamodb?.sequenceNumber?.let { newWatermark ->
            shardWatermarks[shardId] = newWatermark
        }

        if (getRecordsResult.nextShardIterator != null) {
            shardIterators[shardId] = getRecordsResult.nextShardIterator
        } else {
            log.info("ShardEnd: Reached end of shard {}", shardId)
            onShardCompleted(shardId)
        }

        return PollResult(
            sourceRecords,
            reachedShardEnd = getRecordsResult.nextShardIterator == null
        )
    }

    /**
     * Updates [readyShardIds], to have IDs of all shards, which following lineage
     * of shards are ready for consumption (that is shards, which either don't have
     * parents, or their parents were already completed). Processing shards in
     * lineage guarantees consuming changes in-order.
     *
     * Ready shards can change when new shards appear or when processing one of
     * existing shards was completed (shard has been closed and all messages
     * were processed).
     */
    private fun updateReadyShardIds() {
        // Walk through graph from roots in BFS style.
        // If shard is completed, visit its children. Collect all visited,
        // not-completed shards. These are your shards ready for consumption.

        val readyShardIds = HashSet<ShardId>()
        val shardsToVisit: Queue<ShardId> = LinkedList<ShardId>()
        shardsToVisit.addAll(rootShardIds)

        // Note that DAG is free of cycles, hence below loop will end.
        while (shardsToVisit.isNotEmpty()) {
            val visitedShardId: ShardId = shardsToVisit.remove()
            if (completedShardIds.contains(visitedShardId)) {
                shardsToVisit.addAll(graph.successors(visitedShardId))
            } else {
                readyShardIds.add(visitedShardId)
            }
        }

        // Sort ready shards by required wait time ascending, with nulls first, to optimize
        // traversing shards with minimum latency
        this.readyShardIds = readyShardIds
            .sortedWith(nullsFirst(compareBy { shardIntervalLimiters[it]?.timeUntilNext() }))
            .toList()
        this.roundRobinShardIndex = -1

        log.info("ReadyShards: {}", this.readyShardIds.joinToString(separator = ", "))
    }

    /**
     * Returns Kafka Connect source offset for record.
     *
     * Since our partition is entire stream, we give Kafka Connect watermarks
     * for roughly all shards we keep track of as partition offset.
     */
    private fun getSourceOffsetForRecord(shardId: ShardId, sequenceNumber: SequenceNumber): Map<String, String> {
        val sourceOffset = HashMap(shardWatermarks)
        sourceOffset[shardId] = sequenceNumber
        return sourceOffset
    }

    private fun getRecords(shardId: ShardId): GetRecordsResult {
        val shardIterator = getShardIterator(shardId)
        val getRecordsRequest = GetRecordsRequest()
            .withShardIterator(shardIterator)
            .withLimit(GET_RECORDS_MAX_LIMIT) // DynamoDB recommends using highest value
        return streamsClient.getRecords(getRecordsRequest)
    }

    private fun getShardIterator(shardId: ShardId): ShardIterator {
        // Check if we have shard iterator already available. If yes, return that.
        // Note: Keeping track of iterator age to not return expired ones would be just
        //       an optimization. We still have to handle expired iterators up, as there
        //       may be race condition, where data to which iterator pointed was trimmed
        //       before we poll records.
        val existingIterator = shardIterators[shardId]
        if (existingIterator != null) {
            return existingIterator
        }

        // If there is no iterator, send request to get iterator.
        val getIteratorRequest = GetShardIteratorRequest()
            .withStreamArn(streamArn)
            .withShardId(shardId)

        val watermark = shardWatermarks[shardId]
        val getIteratorResult = if (watermark != null) {
            getIteratorRequest
                .withShardIteratorType(ShardIteratorType.AFTER_SEQUENCE_NUMBER)
                .withSequenceNumber(watermark)

            try {
                streamsClient.getShardIterator(getIteratorRequest)
            } catch (ex: TrimmedDataAccessException) {
                log.warn(
                    "TrimmedData: Data in shard {} right after sequence number {} was already trimmed. " +
                        "Most probably some records were lost. Will continue processing from current " +
                        "${ShardIteratorType.TRIM_HORIZON}",
                    shardId, watermark
                )
                streamsClient.getShardIterator(
                    GetShardIteratorRequest()
                        .withStreamArn(streamArn)
                        .withShardId(shardId)
                        .withShardIteratorType(ShardIteratorType.TRIM_HORIZON)
                )
            }
        } else {
            val initialPosition = if (rootShardIds.contains(shardId)) {
                rootShardsInitialPosition
            } else ShardIteratorType.TRIM_HORIZON // Child shards must be consumed from the beginning

            streamsClient.getShardIterator(getIteratorRequest.withShardIteratorType(initialPosition))
        }

        return getIteratorResult.shardIterator
    }

    /**
     * To be called when shard we were consuming got closed and has been fully consumed.
     */
    private fun onShardCompleted(shardId: ShardId) {
        shardIterators.remove(shardId)
        completedShardIds.add(shardId)
        cleanUpNotNeededShards()
        updateReadyShardIds()
    }

    /**
     * Removes no longer needed shards from graph and state.
     *
     * We consider shard not needed, when it has children and all its children were completed
     * (are closed and were fully consumed). We keep parents of not fully consumed shards,
     * so that:
     * - We can preserve lineage even if children shards are not added in one go.
     *   Otherwise the second child would not necessarily have parent, what would result
     *   in exception on adding the second child shard.
     * - Child shards do not become root shards, in which case we would not necessarily
     *   start consuming those from beginning (TRIM_HORIZON).
     */
    private fun cleanUpNotNeededShards() {
        val notNeededShardIds = graph.nodes()
            .filter { shardId ->
                val children = graph.successors(shardId)
                children.isNotEmpty() && children.all { completedShardIds.contains(it) }
            }
            .toSet()

        notNeededShardIds.forEach { notNeededShardId ->
            // Update roots
            rootShardIds.remove(notNeededShardId)
            graph.successors(notNeededShardId)
                .filterNot { childShardId -> notNeededShardId.contains(childShardId) }
                .forEach { childShardId -> rootShardIds.add(childShardId) }

            // Remove shard
            graph.removeNode(notNeededShardId)
            completedShardIds.remove(notNeededShardId)
            shardIterators.remove(notNeededShardId)
            shardWatermarks.remove(notNeededShardId)
            shardIntervalLimiters.remove(notNeededShardId)
        }
    }

    /**
     * Returns true if any of shard's descendants were (not necessarily
     * fully) consumed - that is if we have an offset/watermark stored
     * for those.
     */
    private fun hasConsumedDescendants(shardId: ShardId): Boolean {
        val shardsToVisit: Queue<ShardId> = LinkedList<ShardId>()
        shardsToVisit.addAll(graph.successors(shardId))

        // Note that DAG is free of cycles, hence below loop will end.
        while (shardsToVisit.isNotEmpty()) {
            val visitedShardId: ShardId = shardsToVisit.remove()
            if (shardWatermarks[visitedShardId] != null) {
                return true // We have consumed records from this descendant
            } else {
                shardsToVisit.addAll(graph.successors(visitedShardId))
            }
        }

        return false // We didn't find any descendants, which were already consumed
    }

    companion object {
        private val log = LoggerFactory.getLogger(StreamShardsConsumer::class.java)

        private const val GET_RECORDS_MAX_LIMIT = 1000
    }

    class PollResult(

        /**
         * Records received when polling shard. May be empty.
         */
        val records: List<StreamsRecord>,

        /**
         * Whether we have reached end of shard when polling.
         */
        val reachedShardEnd: Boolean
    )
}