package com.nexxiot.kafka.connect.dynamodb

import com.amazonaws.services.dynamodbv2.model.ShardIteratorType
import org.apache.kafka.common.config.AbstractConfig
import org.apache.kafka.common.config.ConfigDef
import java.time.Duration

open class DynamoDbSourceConnectorConfig(
    props: Map<String, String>
) : AbstractConfig(configDefinition(), props) {

    val streamArn: String
        get() = getString(STREAM_ARN)

    val kafkaTopic: String
        get() = getString(KAFKA_TOPIC)

    val initialIteratorPosition: ShardIteratorType
        get() = ShardIteratorType.fromValue(getString(INITIAL_POSITION))

    val minGetRecordsInterval: Duration
        get() = Duration.ofMillis(getLong(GET_RECORDS_MIN_INTERVAL_MS))

    val mockEndpointUrl: String? = props["streams.mock.endpoint.url"]

    companion object {
        private const val STREAM_ARN = "streams.arn"
        private const val KAFKA_TOPIC = "kafka.topic"
        private const val INITIAL_POSITION = "streams.initial.position"
        private const val GET_RECORDS_MIN_INTERVAL_MS = "streams.get.records.min.interval.ms"

        fun configDefinition(): ConfigDef {
            return ConfigDef()
                .define(
                    STREAM_ARN,
                    ConfigDef.Type.STRING,
                    ConfigDef.NO_DEFAULT_VALUE,
                    ConfigDef.Importance.HIGH,
                    "DynamoDB Stream ARN (Amazon Resource Name) of stream to consume by this " +
                        "connector. You can find currently active table stream's ARN in AWS DynamoDB Console " +
                        "in table overview or fetch it using AWS CLI or SDK. Note that when you disable " +
                        "and re-enable stream on table, a new stream with new ARN will be created. If you want " +
                        "to consume it, you will need to either re-configure this connector or provision a new one."
                )
                .define(
                    KAFKA_TOPIC,
                    ConfigDef.Type.STRING,
                    ConfigDef.NO_DEFAULT_VALUE,
                    ConfigDef.Importance.HIGH,
                    "Name of Kafka sink topic, to which captured records will be written."
                )
                .define(
                    INITIAL_POSITION,
                    ConfigDef.Type.STRING,
                    ShardIteratorType.TRIM_HORIZON.toString(),
                    ConfigDef.Importance.HIGH, // TODO Provide validator
                    "Specifies position in the stream to reset to if no offsets are stored. " +
                        "One of: ${ShardIteratorType.TRIM_HORIZON} (default) or ${ShardIteratorType.LATEST}"
                )
                .define(
                    GET_RECORDS_MIN_INTERVAL_MS,
                    ConfigDef.Type.LONG,
                    1000L,
                    ConfigDef.Importance.MEDIUM,
                    "Defines minimum interval between consecutive GetRecords calls per shard, " +
                        "effectively driving rate at which calls are made. For example, when default value " +
                        "1000 is used, the connector will not make GetRecords calls more often than once " +
                        "every second for concurrently consumed shard. When 2 shards are processed concurrently, " +
                        "the connector will make at most 2 such calls per second - one for each shard." +
                        "\n\n" +
                        "When choosing this value, you are making a trade-off between cost and latency. Higher " +
                        "value will result in less calls and therefore less costs (see DynamoDB Streams pricing), " +
                        "but will impact latency between making change in DynamoDB and seeing it in Kafka. Too " +
                        "high value may cause lagging behind, as single call returns at most 1000 records or 1MB, " +
                        "whichever is hit first." +
                        "\n\n" +
                        "The value specified here is a minimum interval. The effective interval depends on number " +
                        "of concurrently processed shards and DynamoDB Streams API response time. The default is " +
                        "1000 (1 second)."
                )
        }
    }
}