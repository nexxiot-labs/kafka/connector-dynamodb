package com.nexxiot.kafka.connect.dynamodb.client

import com.amazonaws.AmazonClientException
import com.amazonaws.AmazonServiceException
import com.amazonaws.retry.RetryUtils
import com.amazonaws.services.dynamodbv2.model.LimitExceededException
import java.io.IOException
import java.time.Duration
import kotlin.math.min

/**
 * Based on [com.amazonaws.retry.RetryPolicy]. Unfortunately, AWS offered
 * classes are meant to be used by Amazon's HTTP client and not really
 * outside of it, hence we have our own tailored implementation.
 */
internal class RetryPolicy(
    private val baseDelay: Duration,
    private val maxBackoff: Duration
) {

    /**
     * Returns true if we should attempt to retry the request. False otherwise.
     */
    fun isRetryable(exception: AmazonClientException): Boolean {
        // Based on com.amazonaws.retry.PredefinedRetryPolicies.SDKDefaultRetryCondition

        // Always retry on client exceptions caused by IOException
        if (exception.cause is IOException) return true

        // Only retry on a subset of service exceptions
        if (exception is AmazonServiceException) {
            return RetryUtils.isRetryableServiceException(exception)
                || RetryUtils.isThrottlingException(exception)
                || exception is LimitExceededException
                || RetryUtils.isClockSkewError(exception)
        }

        return false
    }

    /**
     * How long to wait before re-trying failed request based on number
     * of [retriesAttempted].
     */
    fun delayBeforeRetry(retriesAttempted: Int): Duration {
        val retries = min(
            retriesAttempted,
            MAX_RETRIES
        )
        val exponentialBackoffMillis = (1L shl retries) * baseDelay.toMillis()
        val cappedBackoffMillis = min(exponentialBackoffMillis, maxBackoff.toMillis())
        return Duration.ofMillis(cappedBackoffMillis)
    }

    companion object {
        /** Maximum retry limit. Avoids overflow issues when shifting bits. **/
        private const val MAX_RETRIES = 30
    }
}