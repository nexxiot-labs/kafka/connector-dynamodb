package com.nexxiot.kafka.connect.dynamodb.client

class GetShardsResult(
    val shards: List<Shard>,
    val hasNextPage: Boolean
) {
    val lastEvaluatedShardId: String?
        get() = shards.lastOrNull()?.id
}