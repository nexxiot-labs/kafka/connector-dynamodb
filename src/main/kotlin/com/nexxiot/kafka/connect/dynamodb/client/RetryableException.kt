package com.nexxiot.kafka.connect.dynamodb.client

import com.amazonaws.AmazonClientException
import java.time.Duration

class RetryableException(
    operation: String,
    override val cause: AmazonClientException?,
    /** How much time must elapse before next try can be attempted. */
    val remainingBackoff: Duration
) : RuntimeException(
    if (cause != null) {
        "An error occurred (${cause.javaClass.simpleName}) when calling $operation operation.  " +
            "Backing-off for $remainingBackoff. Error: ${cause.message}"
    } else "Backoff: Won't retry operation for the next $remainingBackoff", cause
) {
    val returnedControlWithoutTrying: Boolean
        get() = cause == null
}