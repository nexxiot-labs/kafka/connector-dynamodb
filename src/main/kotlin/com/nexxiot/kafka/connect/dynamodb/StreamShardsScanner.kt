package com.nexxiot.kafka.connect.dynamodb

import com.nexxiot.kafka.connect.dynamodb.client.RetryableException
import com.nexxiot.kafka.connect.dynamodb.client.Shard
import com.nexxiot.kafka.connect.dynamodb.client.StreamsClient
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.time.Duration
import java.time.Instant
import java.util.LinkedList
import java.util.concurrent.atomic.AtomicBoolean
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

/**
 * Shard scanner detaches polling AWS APIs to discover new shards from
 * returning the captured shards. It is a thread-safe implementation, which
 * allows you to schedule capturing new shards on one thread and consume
 * the captured shards on another. It keeps track of the last evaluated shard
 * ID to fetch new shards incrementally.
 */
class StreamShardsScanner(
    private val streamsClient: StreamsClient,
    private val streamArn: String,
    private var lastEvaluatedShardId: String?
) {

    /**
     * A buffer to store discovered shards before they are consumed by
     * another thread.
     */
    private val capturedShards: LinkedList<Shard> = LinkedList()

    /**
     * It may happen, that even though stream is active and we reached shard's
     * end, DescribeStream does not return the shard's children (see
     * https://github.com/awslabs/dynamodb-streams-kinesis-adapter/issues/20 ).
     * Intensify DescribeStream calls for the next X seconds to reduce latency
     * in discovering new shards.
     */
    private val hotScanDuration: Duration = Duration.ofSeconds(10)
    private val hotScanIntervalNanos: Long = Duration.ofMillis(250).toNanos()
    private val coldScanIntervalNanos: Long = Duration.ofSeconds(1).toNanos()
    private var hotScanDeadline: Instant = Instant.now()

    private val wakeupLock = ReentrantLock()
    private val wakeupSignal = wakeupLock.newCondition()
    private val shutdown: AtomicBoolean = AtomicBoolean(false)

    /**
     * Periodically polls AWS for new stream's shards. The discovered shards
     * are put in a buffer and can be retrieved with [pollCapturedShards].
     * To minimize shard discovery latency, use [notifyShardEndReached] in
     * addition.
     */
    fun captureNewShardsPeriodically() {
        while (!shutdown.get()) {
            val sleepTimeNanos = try {
                captureNewShards()

                // Select sleep time based on whether we are in a hot period or not
                val isColdPeriod = Instant.now().isAfter(hotScanDeadline)
                if (isColdPeriod) {
                    coldScanIntervalNanos
                } else hotScanIntervalNanos
            } catch (ex: RetryableException) {
                if (!ex.returnedControlWithoutTrying) {
                    log.warn("RetryableException: ${ex.message}", ex)
                }
                ex.remainingBackoff.toNanos()
            }

            wakeupLock.withLock {
                if (!shutdown.get()) {
                    wakeupSignal.awaitNanos(sleepTimeNanos)
                }
            }
        }
    }

    /**
     * As in a queue, retrieves and clears list of shards captured so far.
     * If there were no new shards discovered since the last call, the method
     * returns an empty list.
     */
    fun pollCapturedShards(): List<Shard> {
        val shards = LinkedList<Shard>()
        synchronized(capturedShards) {
            shards.addAll(capturedShards)
            capturedShards.clear()
        }
        return shards
    }

    /**
     * Notifies scanner, that consumer reached end of shard. When consuming head
     * of stream, it is an important milestone. The milestone indicates, that the
     * shard has been closed and its children are already visible in DescribeStream
     * results, or soon will become visible. Scanner uses this notification to
     * minimize shard discovery latency.
     */
    fun notifyShardEndReached() {
        hotScanDeadline = Instant.now().plus(hotScanDuration)
        wakeupLock.withLock {
            wakeupSignal.signal()
        }
    }

    /**
     * Notifies [captureNewShardsPeriodically] to stop execution. The method is
     * asynchronous - it may returns before [captureNewShardsPeriodically]
     * has finished.
     */
    fun shutdown() {
        wakeupLock.withLock {
            shutdown.set(true)
            wakeupSignal.signal()
        }
    }

    /**
     * Fetches shards, which appeared since the last call, once. Newly discovered
     * shards are added to [capturedShards]. If there are multiple pages to go
     * through, the method adds discovered shards to [capturedShards] before
     * fetching the next page. The method returns only once all pages are fetched
     * or an error occurred.
     */
    private fun captureNewShards() {
        do {
            val getShardsResult = streamsClient.getShards(streamArn, lastEvaluatedShardId)
            if (getShardsResult.shards.isNotEmpty()) {
                log.info(
                    getShardsResult.shards.joinToString(
                        prefix = "Shards: Discovered new shards: ",
                        separator = ", "
                    )
                )
                synchronized(capturedShards) {
                    capturedShards.addAll(getShardsResult.shards)
                }
            }
            getShardsResult.lastEvaluatedShardId?.let { lastEvaluatedShardId = it }
        } while (getShardsResult.hasNextPage && !shutdown.get())
    }

    companion object {
        private val log: Logger = LoggerFactory.getLogger(StreamShardsScanner::class.java)
    }
}