package com.nexxiot.kafka.connect.dynamodb

import org.apache.kafka.common.config.ConfigDef
import org.apache.kafka.connect.connector.Task
import org.apache.kafka.connect.source.SourceConnector

@Suppress("unused")
class DynamoDbSourceConnector : SourceConnector() {

    private lateinit var config: DynamoDbSourceConnectorConfig

    override fun start(props: Map<String, String>) {
        config = DynamoDbSourceConnectorConfig(props)
        // All the heavy-lifting is done in task

        // TODO Check the stream and fail if cannot access. Just so that connector doesn't get created with wrong configuration
    }

    override fun taskConfigs(maxTasks: Int): List<Map<String, String>> {
        return listOf(config.originalsStrings())
    }

    override fun stop() {}

    override fun version(): String = Manifest.getVersion()

    override fun taskClass(): Class<out Task> = DynamoDbSourceTask::class.java

    override fun config(): ConfigDef = DynamoDbSourceConnectorConfig.configDefinition()
}