package com.nexxiot.kafka.connect.data

import org.apache.kafka.connect.data.SchemaBuilder

object JavaInstant {
    @Suppress("MemberVisibilityCanBePrivate")
    const val LOGICAL_NAME = "java.util.time.Instant"

    fun builder(): SchemaBuilder = SchemaBuilder.string()
        .name(LOGICAL_NAME)
        .version(1)
}