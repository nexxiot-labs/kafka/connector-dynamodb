package com.nexxiot.kafka.connect.data

import org.apache.kafka.connect.data.Schema
import org.apache.kafka.connect.data.SchemaBuilder

object Json {
    @Suppress("MemberVisibilityCanBePrivate")
    private const val LOGICAL_NAME = "com.nexxiot.kafka.connect.data.Json"

    val SCHEMA: Schema = builder().schema()

    fun builder(): SchemaBuilder = SchemaBuilder.string()
        .name(LOGICAL_NAME)
        .version(1)
}