package com.nexxiot.kafka.connect.dynamodb

import com.amazonaws.services.dynamodbv2.model.AttributeValue
import com.amazonaws.services.dynamodbv2.model.OperationType
import com.amazonaws.services.dynamodbv2.model.Record
import com.amazonaws.services.dynamodbv2.model.StreamRecord
import java.time.Instant
import java.util.Date
import java.util.UUID

data class MockRecord(
    val id: UUID = UUID.randomUUID()
) {
    fun toDynamoDbStreamRecord(sequenceNumber: Int): Record {
        return Record()
            .withEventID(id.toString())
            .withEventName(OperationType.MODIFY.toString())
            .withDynamodb(
                StreamRecord()
                    .withApproximateCreationDateTime(Date.from(Instant.now()))
                    .withSequenceNumber(sequenceNumber.toString())
                    .withKeys(mapOf(PARTITION_KEY_ATTR_NAME to AttributeValue().withS("testKey")))
                    .withNewImage(
                        mapOf(
                            PARTITION_KEY_ATTR_NAME to AttributeValue().withS("testKey"),
                            "value" to AttributeValue().withS("newValue")
                        )
                    )
                    .withOldImage(
                        mapOf(
                            PARTITION_KEY_ATTR_NAME to AttributeValue().withS("testKey"),
                            "value" to AttributeValue().withS("oldValue")
                        )
                    )
            )
    }

    companion object {
        fun generateList(count: Int): List<MockRecord> = (1..count).map { MockRecord() }.toList()

        const val PARTITION_KEY_ATTR_NAME = "key"
    }
}