package com.nexxiot.kafka.connect.dynamodb

import com.google.common.graph.GraphBuilder
import com.google.common.graph.MutableGraph
import io.kotlintest.Matcher
import io.kotlintest.MatcherResult
import java.util.LinkedList
import java.util.Queue
import java.util.UUID

/**
 * The matcher verifies, that collected records match the expected records
 * in expected order. The order is as expected if records:
 * 1. Match order within a single shard.
 * 2. Follow lineage of shards in case shard is closed.
 * 3. Are consumed in any order from all shards ready for consumption in
 *    parallel.
 */
@Suppress("UnstableApiUsage")
class RecordSequenceMatcher {

    private val shardGraph: MutableGraph<Int> = GraphBuilder.directed().build<Int>()

    /**
     * ID of shards, which are ready for consumption following lineage of shards.
     */
    private val readyShards = HashSet<Int>()
    private val consumedShards = HashSet<Int>()
    private val records = HashMap<Int, MutableList<UUID>>()
    private val nextIndex = HashMap<Int, Int>()

    private val recordMatcher = getSingleRecordMatcher()
    private val listMatcher = getListMatcher()

    fun addShard(id: Int, parentId: Int?, expectedRecordIds: List<UUID> = emptyList()) {
        require(parentId == null || shardGraph.nodes().contains(parentId)) {
            "Unknown parent shard $parentId"
        }

        shardGraph.addNode(id)
        if (parentId != null) {
            shardGraph.putEdge(parentId, id)
        }

        if (parentId == null || consumedShards.contains(parentId)) {
            readyShards.add(id)
        }

        records[id] = LinkedList(expectedRecordIds)
        nextIndex[id] = 0
    }

    fun addRecords(shardId: Int, expectedRecordIds: List<UUID>) {
        require(shardGraph.nodes().contains(shardId)) { "Unknown shard $shardId" }
        records[shardId]!!.addAll(expectedRecordIds)
    }

    fun matchNext(): Matcher<UUID> = recordMatcher

    fun matchAllNext(): Matcher<List<UUID>?> = listMatcher

    fun hasAnyOutstandingRecords(): Boolean = getOutstandingRecordsCount() > 0

    fun getOutstandingRecordsCount(): Int {
        var outstandingRecordsTotal = 0
        val shardsToVisit: Queue<Int> = LinkedList<Int>(readyShards)
        while (shardsToVisit.isNotEmpty()) {
            val shardId = shardsToVisit.remove()
            val outstandingRecordsCount = records[shardId]!!.size - nextIndex[shardId]!!
            outstandingRecordsTotal += outstandingRecordsCount
            shardsToVisit.addAll(shardGraph.successors(shardId))
        }
        return outstandingRecordsTotal
    }

    private fun getSingleRecordMatcher() = object : Matcher<UUID> {
        override fun test(value: UUID): MatcherResult {
            // Check if there are any already consumed shards - if yes, update ready shards
            do {
                val finishedShards = readyShards
                    .filter { shardGraph.successors(it).isNotEmpty() } // has children
                    .filter { nextIndex[it]!! == records[it]!!.size } // all available records consumed
                    .toSet()

                consumedShards.addAll(finishedShards)
                readyShards.removeAll(finishedShards)
                finishedShards.forEach { readyShards.addAll(shardGraph.successors(it)) }
            } while (finishedShards.isNotEmpty()) // loop in case there are any empty finished shards

            if (readyShards.isEmpty()) {
                return notPassedMatch { "Message $value is not expected - there are no shards ready to consume" }
            }

            // Find shard, for which record with given ID is the next one
            val consumedShardId = readyShards
                .asSequence()
                .filter { nextIndex[it]!! < records[it]!!.size } // there are records to be consumed
                .map { shardId ->
                    val nextRecordId = records[shardId]!![nextIndex[shardId]!!]
                    return@map Pair(shardId, nextRecordId)
                }
                .filter { (_, nextRecordId) -> nextRecordId == value }
                .map { (shardId, _) -> shardId }
                .singleOrNull()

            return if (consumedShardId != null) {
                // Found the record
                nextIndex[consumedShardId] = nextIndex[consumedShardId]!! + 1
                passedMatch()
            } else {
                // Does not match any record in expected sequence
                notPassedMatch {
                    "Message $value is not expected - none of ready shards has it as next message in sequence"
                }
            }
        }
    }

    private fun getListMatcher() = object : Matcher<List<UUID>?> {
        override fun test(value: List<UUID>?): MatcherResult {
            if (value.isNullOrEmpty()) {
                return passedMatch()
            }

            val singleRecordMatcher = matchNext()
            value.forEach { expectedRecordId ->
                val testResult = singleRecordMatcher.test(expectedRecordId)
                if (!testResult.passed()) {
                    return testResult
                }
            }

            // All matched
            return passedMatch()
        }
    }
}