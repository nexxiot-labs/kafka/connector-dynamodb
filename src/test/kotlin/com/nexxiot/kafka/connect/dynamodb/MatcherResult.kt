package com.nexxiot.kafka.connect.dynamodb

import io.kotlintest.MatcherResult

fun passedMatch(): MatcherResult {
    return MatcherResult.invoke(
        true,
        { throw NotImplementedError("Wasn't needed so far") },
        { throw NotImplementedError("Wasn't needed so far") }
    )
}

fun notPassedMatch(failureMsg: () -> String): MatcherResult {
    return MatcherResult.invoke(
        false,
        failureMsg,
        { throw NotImplementedError("Wasn't needed so far") }
    )
}