package com.nexxiot.kafka.connect.dynamodb

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.ObjectWriter
import com.flipkart.zjsonpatch.JsonDiff
import io.kotlintest.Matcher
import io.kotlintest.MatcherResult

private object JsonMatcher {
    val DEFAULT_MAPPER: ObjectMapper = ObjectMapper()
    val PRETTY_PRINTER: ObjectWriter = DEFAULT_MAPPER.writerWithDefaultPrettyPrinter()
}

/**
 * Tests, whether value is structurally the same as expected value.
 *
 * Two JSON strings are structurally the same if they have the same
 * fields with the same values. Field order and whitespace outside
 * of keys and values do not matter.
 */
fun equalJson(expectedJson: String): Matcher<String> = object : Matcher<String> {
    override fun test(value: String): MatcherResult {
        val expected = JsonMatcher.DEFAULT_MAPPER.readTree(expectedJson)
        val actual = JsonMatcher.DEFAULT_MAPPER.readTree(value)

        return if (actual == expected) passedMatch() else notPassedMatch {
            val patchNode = JsonDiff.asJson(expected, actual)
            val prettyDiff = JsonMatcher.PRETTY_PRINTER.writeValueAsString(patchNode)
            val prettyActualJson = JsonMatcher.PRETTY_PRINTER.writeValueAsString(actual)
            """
                JSON does not match the expected value. Difference (JSON Patch):
                $prettyDiff
                Given JSON:
                $prettyActualJson
            """.trimIndent()
        }
    }
}
