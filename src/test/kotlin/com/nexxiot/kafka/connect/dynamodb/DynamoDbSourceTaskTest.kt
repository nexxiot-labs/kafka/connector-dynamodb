package com.nexxiot.kafka.connect.dynamodb

import com.nexxiot.kafka.connect.dynamodb.utils.IntervalLimiter
import io.kotlintest.IsolationMode
import io.kotlintest.Matcher
import io.kotlintest.MatcherResult
import io.kotlintest.matchers.collections.shouldContainExactly
import io.kotlintest.matchers.maps.shouldContainKey
import io.kotlintest.matchers.numerics.shouldBeLessThan
import io.kotlintest.should
import io.kotlintest.shouldBe
import io.kotlintest.shouldNotBe
import io.kotlintest.specs.StringSpec
import kotlinx.coroutines.delay
import org.apache.kafka.connect.data.Schema
import org.apache.kafka.connect.data.Struct
import org.apache.kafka.connect.source.SourceRecord
import org.apache.kafka.connect.source.SourceTask
import java.lang.management.ManagementFactory
import java.time.Duration
import java.util.LinkedList
import java.util.UUID

@Suppress("BlockingMethodInNonBlockingContext")
class DynamoDbSourceTaskTest : StringSpec() {

    override fun isolationMode() = IsolationMode.InstancePerLeaf

    private val streamArn = "mock:stream"
    private val mockStreams = MockDynamoDbStreams(streamArn)
    private val task: SourceTask = DynamoDbSourceTask(mockStreams)
    private val taskContext = MockSourceTaskContext()
    private val expectedRecords = RecordSequenceMatcher()

    override fun closeResources() {
        task.stop() // async, so watch out if you close other resources
        super.closeResources()
    }

    init {
        task.initialize(taskContext)

        "Consume - just one shard - all records are consumed" {
            val shardRecords = MockRecord.generateList(3)
            mockStreams.addShard(1, null, shardRecords)
            expectedRecords.addShard(1, null, shardRecords.ids())

            task.start(mockConfig())
            val polledRecords = task.poll()

            polledRecords.extractIds() should expectedRecords.matchAllNext()
            expectedRecords.hasAnyOutstandingRecords() shouldBe false

            // Poll again when there is no new data
            val polledRecords2 = task.poll()
            polledRecords2 shouldBe null

            // Add more records to shard
            val addedRecords = MockRecord.generateList(2)
            mockStreams.addRecordsToShard(1, addedRecords)
            expectedRecords.addRecords(1, addedRecords.ids())

            val polledAddedRecords = task.poll()
            polledAddedRecords.extractIds() should expectedRecords.matchAllNext()
            expectedRecords.hasAnyOutstandingRecords() shouldBe false
        }

        "Consume - one shard with one record - record has expected format" {
            val shardRecords = MockRecord.generateList(1)
            mockStreams.addShard(1, null, shardRecords)

            task.start(mockConfig())
            val polledRecords = task.poll()

            polledRecords?.size shouldBe 1
            val record = polledRecords[0]

            record.keySchema().type() shouldBe Schema.Type.STRING
            val key = record.key() as? String
            key shouldBe "testKey"

            record.valueSchema().type() shouldBe Schema.Type.STRUCT
            val fieldNames = record.valueSchema().fields().map { it.name() }.toSet()
            fieldNames shouldContainExactly setOf(
                "id", "approx_time", "type", "keys", "new_image", "old_image", "discover_time"
            )
            record.valueSchema().field("id").schema().type() shouldBe Schema.Type.STRING
            record.valueSchema().field("approx_time").schema().type() shouldBe Schema.Type.STRING
            record.valueSchema().field("approx_time").schema().name() shouldBe "java.util.time.Instant"
            record.valueSchema().field("type").schema().type() shouldBe Schema.Type.STRING
            record.valueSchema().field("keys").schema().type() shouldBe Schema.Type.STRING
            record.valueSchema().field("keys").schema().name() shouldBe "com.nexxiot.kafka.connect.data.Json"
            record.valueSchema().field("new_image").schema().type() shouldBe Schema.Type.STRING
            record.valueSchema().field("new_image").schema().name() shouldBe "com.nexxiot.kafka.connect.data.Json"
            record.valueSchema().field("old_image").schema().type() shouldBe Schema.Type.STRING
            record.valueSchema().field("old_image").schema().name() shouldBe "com.nexxiot.kafka.connect.data.Json"
            record.valueSchema().field("discover_time").schema().type() shouldBe Schema.Type.STRING
            record.valueSchema().field("discover_time").schema().name() shouldBe "java.util.time.Instant"

            val value = record.value() as? Struct
            value shouldNotBe null
            (value!!["type"] as String) shouldBe "MODIFY"
            (value["keys"] as String) should equalJson("""{"key": "testKey"}""")
            (value["new_image"] as String) should equalJson("""{"key": "testKey", "value": "newValue"}""")
            (value["old_image"] as String) should equalJson("""{"key": "testKey", "value": "oldValue"}""")
        }

        "Consume - parent shard with single child - all records are consumed in expected order" {
            val parentShardRecords = MockRecord.generateList(3)
            mockStreams.addShard(1, null, parentShardRecords, closed = true)
            expectedRecords.addShard(1, null, parentShardRecords.ids())

            val childShardRecords = MockRecord.generateList(2)
            mockStreams.addShard(2, 1, childShardRecords)
            expectedRecords.addShard(2, 1, childShardRecords.ids())

            task.start(mockConfig())
            task.pollUntilCollectsAllExpectedRecords()
        }

        "Consume - parent shard with 2 children - all records are consumed in expected order" {
            val parentShardRecords = MockRecord.generateList(3)
            mockStreams.addShard(1, null, parentShardRecords, closed = true)
            expectedRecords.addShard(1, null, parentShardRecords.ids())

            val firstChildShardRecords = MockRecord.generateList(6)
            mockStreams.addShard(2, 1, firstChildShardRecords)
            expectedRecords.addShard(2, 1, firstChildShardRecords.ids())

            val secondChildShardRecords = MockRecord.generateList(4)
            mockStreams.addShard(3, 1, secondChildShardRecords)
            expectedRecords.addShard(3, 1, secondChildShardRecords.ids())

            task.start(mockConfig())
            task.pollUntilCollectsAllExpectedRecords()
        }

        "Consume - 2 concurrent shards - records are consumed from both shards simultaneously" {
            val shard1Records = MockRecord.generateList(2)
            mockStreams.addShard(1, null, shard1Records)
            expectedRecords.addShard(1, null, shard1Records.ids())

            val shard2Records = MockRecord.generateList(3)
            mockStreams.addShard(2, null, shard2Records)
            expectedRecords.addShard(2, null, shard2Records.ids())

            task.start(mockConfig())
            task.pollUntilCollectsAllExpectedRecords()

            val shard1NewRecords = MockRecord.generateList(5)
            mockStreams.addRecordsToShard(1, shard1NewRecords)
            expectedRecords.addRecords(1, shard1NewRecords.ids())

            val shard2NewRecords = MockRecord.generateList(1)
            mockStreams.addRecordsToShard(2, shard2NewRecords)
            expectedRecords.addRecords(2, shard2NewRecords.ids())

            task.pollUntilCollectsAllExpectedRecords()
        }

        "Consume - parent shard first closes, then child is added - all records are consumed in expected order" {
            val parentShardRecords = MockRecord.generateList(3)
            mockStreams.addShard(1, null, parentShardRecords)

            task.start(mockConfig())
            val polledRecords = task.poll()
            polledRecords?.size shouldBe 3

            // Poll 2 times after shard was closed
            mockStreams.closeShard(1)
            val polledRecords2 = task.poll()
            polledRecords2 shouldBe null
            val polledRecords3 = task.poll()
            polledRecords3 shouldBe null

            // Add child shard
            val childShardRecords = MockRecord.generateList(2)
            mockStreams.addShard(2, 1, childShardRecords)
            expectedRecords.addShard(2, null, childShardRecords.ids())

            // Expect all shards to be consumed
            task.pollUntilCollectsAllExpectedRecords()
        }

        "Consume - first child shard is added, then parent closes - all records are consumed in expected order" {
            val parentShardRecords = MockRecord.generateList(3)
            mockStreams.addShard(1, null, parentShardRecords)

            task.start(mockConfig())
            val polledRecords = task.poll()
            polledRecords?.size shouldBe 3

            // Add child shard and wait for task to discover it
            val childShardRecords = MockRecord.generateList(2)
            mockStreams.addShard(2, 1, childShardRecords)
            expectedRecords.addShard(2, null, childShardRecords.ids())
            delay(Duration.ofSeconds(3).toMillis())

            // Poll again - shouldn't continue to new shard yet
            val polledRecords2 = task.poll()
            polledRecords2 shouldBe null

            // Close shard and poll 2 times - once to detect shard end and once to consume second shard
            mockStreams.closeShard(1)
            val polledRemainingParentRecords = task.poll()
            polledRemainingParentRecords shouldBe null
            val polledChildRecords = task.poll()
            polledChildRecords.extractIds() should expectedRecords.matchAllNext()
            expectedRecords.hasAnyOutstandingRecords() shouldBe false
        }

        "Consume - second child appears after first child was already used - all records are consumed in expected order" {
            val parentShardRecords = MockRecord.generateList(3)
            mockStreams.addShard(1, null, parentShardRecords, closed = true)
            expectedRecords.addShard(1, null, parentShardRecords.ids())
            val firstChildRecords = MockRecord.generateList(2)
            mockStreams.addShard(2, 1, firstChildRecords)
            expectedRecords.addShard(2, 1, firstChildRecords.ids())

            task.start(mockConfig())
            task.pollUntilCollectsAllExpectedRecords()

            // Add second child shard and wait for task to discover it
            val secondChildShardRecords = MockRecord.generateList(1)
            mockStreams.addShard(3, 1, secondChildShardRecords)
            expectedRecords.addShard(3, 1, secondChildShardRecords.ids())
            delay(Duration.ofSeconds(3).toMillis())

            task.pollUntilCollectsAllExpectedRecords()
        }

        "Consume - child and grandchild appear at the same time - all records are consumed in expected order" {
            val parentShardRecords = MockRecord.generateList(3)
            mockStreams.addShard(1, null, parentShardRecords)
            expectedRecords.addShard(1, null, parentShardRecords.ids())

            task.start(mockConfig())
            val polledRecords = task.poll()

            polledRecords.extractIds() should expectedRecords.matchAllNext()
            expectedRecords.hasAnyOutstandingRecords() shouldBe false

            // Add 2 shards at the same time: child and grandchild
            // There is a risk that scan will discover shard in-between adding those,
            // In which case the desired scenario will not be tested. It should be
            // tested most of the time though.
            val childShardRecords = emptyList<MockRecord>()
            mockStreams.closeShard(1)
            mockStreams.addShard(2, 1, childShardRecords, closed = true)
            expectedRecords.addShard(2, 1, childShardRecords.ids())
            val grandchildShardRecords = MockRecord.generateList(2)
            mockStreams.addShard(3, 2, grandchildShardRecords)
            expectedRecords.addShard(3, 2, grandchildShardRecords.ids())

            task.pollUntilCollectsAllExpectedRecords()
        }

        "Consume - there are multiple shards with thousands of records - all records are consumed in expected order" {
            // DynamoDB recommends to take maximum amount of records at once, which is 1000
            //
            // The test shards form following graph:
            //   1     2
            //   |    / \
            //   5   3  4
            //       |  |
            //       6  7

            // Add root shard with one child shard
            addShardWithExpectedRecords(1, null, 2700, closed = true)
            addShardWithExpectedRecords(5, 1, 1251)

            // Add another root shard, with 2 children, each with grandchild
            addShardWithExpectedRecords(2, null, 3203, closed = true)
            addShardWithExpectedRecords(3, 2, 1843, closed = true)
            addShardWithExpectedRecords(4, 2, 4187, closed = true)
            addShardWithExpectedRecords(6, 3, 125)
            addShardWithExpectedRecords(7, 4, 1025, closed = true)

            task.start(mockConfig())
            task.pollUntilCollectsAllExpectedRecords(timeout = Duration.ofSeconds(10))
        }

        "Consume - there are many shards to paginate through - all records are consumed in expected order" {
            // We set up out mock to return at most 4 shards on each page
            //
            // Shard form following graph:
            //    1    2    3    4    5    6    7    8
            //    |    |    |    |    |    |    |   / \
            //    9   10   11   12   13   14   15  16 17

            mockStreams.setDescribeStreamsShardsPageLimit(4)

            // Add root shards
            addShardWithExpectedRecords(1, null, 1, closed = true)
            addShardWithExpectedRecords(2, null, 2, closed = true)
            addShardWithExpectedRecords(3, null, 3, closed = true)
            addShardWithExpectedRecords(4, null, 4, closed = true)
            addShardWithExpectedRecords(5, null, 4, closed = true)
            addShardWithExpectedRecords(6, null, 3, closed = true)
            addShardWithExpectedRecords(7, null, 2, closed = true)
            addShardWithExpectedRecords(8, null, 1, closed = true)

            // Get all records from the shards
            task.start(mockConfig())
            task.pollUntilCollectsAllExpectedRecords()

            // Poll again when there are no shards available
            val polledRecords = task.poll()
            polledRecords shouldBe null

            // Add all child shards
            addShardWithExpectedRecords(9, 1, 2)
            addShardWithExpectedRecords(10, 2, 1)
            addShardWithExpectedRecords(11, 3, 1008)
            addShardWithExpectedRecords(12, 4, 3)
            addShardWithExpectedRecords(13, 5, 3)
            addShardWithExpectedRecords(14, 6, 306)
            addShardWithExpectedRecords(15, 7, 3)
            addShardWithExpectedRecords(16, 8, 3)
            addShardWithExpectedRecords(17, 8, 1)

            task.pollUntilCollectsAllExpectedRecords()
        }

        "Consume - configured to start from LATEST - ignores existing records, returns all new records" {
            // Shards graph 1 -> 2 -> 3
            // We start the task, when we are in the middle of shard 2
            // After first poll, we add remaining records to shard 2 and create its child with records
            // We should ignore shard 1 and half of shard 2 and then consume the other half of shard 2
            // and all records in shard 3

            val parentRecords = MockRecord.generateList(3)
            mockStreams.addShard(1, null, parentRecords, closed = true)
            val recordsToIgnore = MockRecord.generateList(2)
            mockStreams.addShard(2, 1, recordsToIgnore)

            task.start(mockConfig(mapOf("streams.initial.position" to "LATEST")))
            val polledRecords = task.poll()
            polledRecords shouldBe null // ignoring records available before start

            val newRecords = MockRecord.generateList(3)
            mockStreams.addRecordsToShard(2, newRecords)
            expectedRecords.addShard(2, null, newRecords.ids())
            mockStreams.closeShard(2)
            val newChildRecords = MockRecord.generateList(4)
            mockStreams.addShard(3, 2, newChildRecords)
            expectedRecords.addShard(3, 2, newChildRecords.ids())

            task.pollUntilCollectsAllExpectedRecords()
        }

        "Consume - iterator expired (e.g. after pausing task) - continues where it left off" {
            val shardRecords = MockRecord.generateList(3)
            mockStreams.addShard(1, null, shardRecords)
            expectedRecords.addShard(1, null, shardRecords.ids())

            // Poll records, so it caches iterator
            task.start(mockConfig())
            val polledRecords = task.poll()
            polledRecords.extractIds() should expectedRecords.matchAllNext()
            expectedRecords.hasAnyOutstandingRecords() shouldBe false

            // Simulate pause - expire iterators
            mockStreams.expireIterators()

            // Add more records to shard
            val addedRecords = MockRecord.generateList(2)
            mockStreams.addRecordsToShard(1, addedRecords)
            expectedRecords.addRecords(1, addedRecords.ids())

            // Expect it got those added records, despite iterator expired
            val polledAddedRecords = task.poll()
            polledAddedRecords.extractIds() should expectedRecords.matchAllNext()
            expectedRecords.hasAnyOutstandingRecords() shouldBe false
        }

        "State - state is built from task-returned offsets, then we stop task and start a new instance - all records are consumed just once" {
            val records = MockRecord.generateList(3)
            mockStreams.addShard(1, null, records)
            expectedRecords.addShard(1, null, records.ids())

            // Consume some records
            task.start(mockConfig())
            val polledRecords = task.poll()
            polledRecords.extractIds() should expectedRecords.matchAllNext()
            expectedRecords.hasAnyOutstandingRecords() shouldBe false

            // Persist state
            val partition = polledRecords.last().sourcePartition()
            val offset = polledRecords.last().sourceOffset()
            taskContext.setOffset(partition, offset)

            // Add records
            val newRecords = MockRecord.generateList(4)
            mockStreams.addRecordsToShard(1, newRecords)
            expectedRecords.addRecords(1, newRecords.ids())

            // Spin up new task, which should resume from persisted state (we keep the other task paused)
            val newTask = DynamoDbSourceTask(mockStreams)
            newTask.initialize(taskContext)
            newTask.start(mockConfig())
            val newTaskPolledRecords = try {
                newTask.poll()
            } finally {
                newTask.stop()
            }

            newTaskPolledRecords.extractIds() should expectedRecords.matchAllNext()
            expectedRecords.hasAnyOutstandingRecords() shouldBe false
        }

        "State - V1, just one shard - records consumed from where we left off" {
            // State is versioned, as it is persisted. When state's schema changes and existing
            // task gets upgraded, it will have to deal with state persisted by some previous
            // task version.

            val stateV1Partition = mapOf("stream_arn" to streamArn)
            val stateV1Offset = mapOf("1" to "2") // we have consumed all records,
            // up to record with sequence number 2 (inclusive)

            taskContext.setOffset(stateV1Partition, stateV1Offset)

            val records = MockRecord.generateList(5)
            mockStreams.addShard(1, null, records)
            expectedRecords.addShard(1, null, records.drop(3).ids()) // inclusive seq number 2, 0-indexed

            task.start(mockConfig())
            val polledRecords = task.poll()

            polledRecords.extractIds() should expectedRecords.matchAllNext()
            expectedRecords.hasAnyOutstandingRecords() shouldBe false
        }

        "State - only parent was already consumed - records consumed from where we left off" {
            val parentRecords = MockRecord.generateList(5)
            mockStreams.addShard(1, null, parentRecords, closed = true)
            val childRecords = MockRecord.generateList(7)
            mockStreams.addShard(2, 1, childRecords)

            val statePartition = mapOf("stream_arn" to streamArn)
            val stateOffset = mapOf("1" to "4") // all parent records were consumed
            taskContext.setOffset(statePartition, stateOffset)

            expectedRecords.addShard(2, null, childRecords.ids())

            task.start(mockConfig())
            val polledRecords = task.poll()

            polledRecords.extractIds() should expectedRecords.matchAllNext()
            expectedRecords.hasAnyOutstandingRecords() shouldBe false
        }

        "State - parent and child were already consumed - records consumed from where we left off" {
            // First get task to consume something and generate state
            val parentRecords = MockRecord.generateList(4)
            mockStreams.addShard(1, null, parentRecords, closed = true)
            val childRecords = MockRecord.generateList(2)
            mockStreams.addShard(2, 1, childRecords)

            task.start(mockConfig())
            val polledParentRecords = task.poll()
            polledParentRecords?.size shouldBe 4
            val polledChildRecords = task.poll()
            polledChildRecords?.size shouldBe 2

            // Persist state
            val partition = polledChildRecords.last().sourcePartition()
            val offset = polledChildRecords.last().sourceOffset()
            taskContext.setOffset(partition, offset)

            // Add records
            val newRecords = MockRecord.generateList(3)
            mockStreams.addRecordsToShard(2, newRecords)
            expectedRecords.addShard(2, null, newRecords.ids())

            // Spin up new task, which should resume from persisted state (we keep the other task paused)
            val newTask = DynamoDbSourceTask(mockStreams)
            newTask.initialize(taskContext)
            newTask.start(mockConfig())
            val newTaskPolledRecords = try {
                newTask.poll()
            } finally {
                newTask.stop()
            }

            newTaskPolledRecords.extractIds() should expectedRecords.matchAllNext()
            expectedRecords.hasAnyOutstandingRecords() shouldBe false
        }

        "State - consumes many shards in line - no longer needed state is cleaned up" {
            addShardWithExpectedRecords(1, null, 3, closed = true)
            addShardWithExpectedRecords(2, 1, 4, closed = true)
            addShardWithExpectedRecords(3, 2, 2, closed = true)
            addShardWithExpectedRecords(4, 3, 1, closed = true)
            addShardWithExpectedRecords(5, 4, 5, closed = true)
            addShardWithExpectedRecords(6, 5, 6, closed = true)
            addShardWithExpectedRecords(7, 6, 3, closed = true)
            addShardWithExpectedRecords(8, 7, 2, closed = true)

            task.start(mockConfig())
            val collectedRecords = task.pollUntilCollectsAllExpectedRecords()
            val offset = collectedRecords.last().sourceOffset()
            offset.size shouldBeLessThan 4
            offset shouldContainKey "8"
        }

        "State - contains no longer available shards - state is cleaned up" {
            val records = MockRecord.generateList(3)
            mockStreams.addShard(10, 9, records)

            // Add state for no longer existing shards
            val statePartition = mapOf("stream_arn" to streamArn)
            val stateOffset = mapOf("1" to "4", "2" to "2")
            taskContext.setOffset(statePartition, stateOffset)

            task.start(mockConfig())
            val polledRecords = task.poll()
            val newState = polledRecords.last().sourceOffset()
            newState shouldContainKey "10"
            newState.size shouldBe 1
        }

        "Trimming - state for no longer available shards is available; configured to start from LATEST - all shards without state are consumed from TRIM_HORIZON" {
            val config = mockConfig(mapOf("streams.initial.position" to "LATEST"))

            // Add state for no longer existing shards
            val statePartition = mapOf("stream_arn" to streamArn)
            val stateOffset = mapOf("1" to "4", "2" to "2")
            taskContext.setOffset(statePartition, stateOffset)

            val shardWithStateRecords = MockRecord.generateList(5)
            mockStreams.addShard(2, 1, shardWithStateRecords)
            expectedRecords.addShard(2, null, shardWithStateRecords.ids().drop(3))
            val shardWithoutStateRecords = MockRecord.generateList(7)
            mockStreams.addShard(3, 1, shardWithoutStateRecords)
            expectedRecords.addShard(3, null, shardWithoutStateRecords.ids())

            task.start(config)
            task.pollUntilCollectsAllExpectedRecords()
        }

        "Trimming - state points to already trimmed records; configured to start from LATEST - continues from shard's TRIM_HORIZON" {
            val records = MockRecord.generateList(10)
            mockStreams.addShard(1, null, records)
            mockStreams.trimShard(1, 6) // Trim first 6 records
            expectedRecords.addShard(1, null, records.ids().drop(6))

            // Add state pointing to trimmed records
            val statePartition = mapOf("stream_arn" to streamArn)
            val stateOffset = mapOf("1" to "3") // state points to 4th record
            taskContext.setOffset(statePartition, stateOffset)

            task.start(mockConfig(mapOf("streams.initial.position" to "LATEST")))

            val polledRecords = task.poll()

            polledRecords.extractIds() should expectedRecords.matchAllNext()
            expectedRecords.hasAnyOutstandingRecords() shouldBe false
        }

        "Trimming - record is trimmed after obtaining iterator, but before getting records; configured to start from LATEST - continues from shard's TRIM_HORIZON" {
            mockStreams.addShard(1, null)
            task.start(mockConfig(mapOf("streams.initial.position" to "LATEST")))

            // Poll records to get iterator for LATEST
            val polledRecords = task.poll()
            polledRecords shouldBe null

            // Add and poll records to keep stream offset
            // Note that if stream didn't have any records, we won't get any offset,
            // so we can't really start from point where task started.
            val recordsBeforeTrimming = MockRecord.generateList(2)
            mockStreams.addRecordsToShard(1, recordsBeforeTrimming)
            val polledRecords2 = task.poll()
            polledRecords2 shouldNotBe null

            // Add records and trim some, so that task's cached iterator points to trimmed offsets
            val recordsToTrim = MockRecord.generateList(4)
            val recordsAfterTrimming = MockRecord.generateList(5)
            mockStreams.addRecordsToShard(1, recordsToTrim)
            mockStreams.addRecordsToShard(1, recordsAfterTrimming)
            mockStreams.trimShard(1, recordsBeforeTrimming.size + recordsToTrim.size)
            expectedRecords.addShard(1, null, recordsAfterTrimming.ids())

            val postTrimPolledRecords = task.poll()

            postTrimPolledRecords shouldNotBe null
            postTrimPolledRecords.extractIds() should expectedRecords.matchAllNext()
            expectedRecords.hasAnyOutstandingRecords() shouldBe false
        }

        "Consume - GetRecords is throttling - continues to work" {
            // Start throttling
            mockStreams.throttleAllShards = true

            // Add shard
            val records = MockRecord.generateList(3)
            mockStreams.addShard(1, null, records)
            expectedRecords.addShard(1, null, records.ids())

            // Start task
            task.start(mockConfig())

            // Poll first time
            val polledResults1 = task.poll()
            polledResults1 shouldBe null

            // Poll second time
            val polledResults2 = task.poll()
            polledResults2 shouldBe null

            mockStreams.throttleAllShards = false

            task.pollUntilCollectsAllExpectedRecords()
        }

        "Consume - DescribeStream is throttling - continues to work" {
            // Add shard
            val parentShardRecords = MockRecord.generateList(3)
            mockStreams.addShard(1, null, parentShardRecords)

            // Start task
            task.start(mockConfig())

            mockStreams.throttleDescribeStream = true

            // Add child shard
            mockStreams.closeShard(1)
            val childShardRecords = MockRecord.generateList(4)
            mockStreams.addShard(2, 1, childShardRecords)

            // Poll for 3 seconds, expect only records from parent shard
            expectedRecords.addShard(1, null, parentShardRecords.ids())
            val deadline = System.nanoTime() + Duration.ofSeconds(3).toNanos()
            val limiter = IntervalLimiter(Duration.ofMillis(25)) // make sure other threads have time to run
            val collectedRecords = LinkedList<SourceRecord>()
            while (System.nanoTime() < deadline) {
                limiter.waitForNext()
                val polledRecords = task.poll()
                if (polledRecords != null) {
                    polledRecords.extractIds() should expectedRecords.matchAllNext()
                    collectedRecords.addAll(polledRecords)
                }
            }
            expectedRecords.hasAnyOutstandingRecords() shouldBe false

            mockStreams.throttleDescribeStream = false

            // Poll until child shard records are collected
            expectedRecords.addShard(2, 1, childShardRecords.ids())
            task.pollUntilCollectsAllExpectedRecords()
        }

        "Consume - Transient network errors during operation - continues to work" {
            // Add shard
            val parentShardRecords = MockRecord.generateList(3)
            mockStreams.addShard(1, null, parentShardRecords)
            expectedRecords.addShard(1, null, parentShardRecords.ids())

            // Start task
            task.start(mockConfig())

            mockStreams.isReachable = false

            // Add child shard
            mockStreams.closeShard(1)
            val childShardRecords = MockRecord.generateList(4)
            mockStreams.addShard(2, 1, childShardRecords)
            expectedRecords.addShard(2, 1, childShardRecords.ids())

            // Poll
            val polledResults1 = task.poll()
            polledResults1 shouldBe null

            // Wait 5 second for describe stream to run a few times
            delay(Duration.ofSeconds(5).toMillis())

            // Poll again
            val polledResults2 = task.poll()
            polledResults2 shouldBe null

            mockStreams.isReachable = true

            task.pollUntilCollectsAllExpectedRecords()
        }
    }

    private fun addShardWithExpectedRecords(shardId: Int, parentId: Int?, recordsCount: Int, closed: Boolean = false) {
        val records = MockRecord.generateList(recordsCount)
        mockStreams.addShard(shardId, parentId, records, closed)
        expectedRecords.addShard(shardId, parentId, records.ids())
    }

    private fun mockConfig(overrides: Map<String, String> = emptyMap()): Map<String, String> {
        val mockConfig = mutableMapOf(
            "streams.arn" to streamArn,
            "kafka.topic" to "test-topic",
            "streams.get.records.min.interval.ms" to "100"
        )
        overrides.forEach { (key, value) -> mockConfig[key] = value }
        return mockConfig
    }

    /**
     * Polls multiple times until either all expected records from [expectedRecords] are
     * collected or [timeout] is reached. Polling multiple times may be required,
     * when multiple shards are involved.
     */
    private fun SourceTask.pollUntilCollectsAllExpectedRecords(
        timeout: Duration = Duration.ofSeconds(5)
    ): List<SourceRecord> {
        val deadline = if (!isDebug) {
            System.nanoTime() + timeout.toNanos()
        } else Long.MAX_VALUE

        val limiter = IntervalLimiter(Duration.ofMillis(25)) // make sure other threads have time to run

        val collectedRecords = LinkedList<SourceRecord>()
        while (expectedRecords.hasAnyOutstandingRecords() && System.nanoTime() < deadline) {
            limiter.waitForNext()
            val polledRecords = this.poll()
            if (polledRecords != null) {
                polledRecords.extractIds() should expectedRecords.matchAllNext()
                collectedRecords.addAll(polledRecords)
            }
        }

        expectedRecords.getOutstandingRecordsCount() should object : Matcher<Int> {
            override fun test(value: Int): MatcherResult {
                return if (value == 0) {
                    passedMatch()
                } else {
                    notPassedMatch {
                        "Got ${collectedRecords.size} records, but expected $value records more. " +
                            "Gave up after ${timeout.seconds}s"
                    }
                }
            }
        }

        return collectedRecords
    }

    private fun List<MockRecord>.ids(): List<UUID> = this.map { it.id }.toList()

    private fun List<SourceRecord>?.extractIds(): List<UUID>? = this?.map {
        val changeEvent = it.value() as Struct
        val idString = changeEvent.getString("id")
        return@map UUID.fromString(idString)
    }?.toList()

    companion object {
        private val isDebug = ManagementFactory.getRuntimeMXBean()
            .inputArguments.toString().contains("-agentlib:jdwp")
    }
}