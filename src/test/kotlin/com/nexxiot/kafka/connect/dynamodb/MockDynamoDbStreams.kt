package com.nexxiot.kafka.connect.dynamodb

import com.amazonaws.AmazonClientException
import com.amazonaws.AmazonServiceException
import com.amazonaws.AmazonWebServiceRequest
import com.amazonaws.ResponseMetadata
import com.amazonaws.http.exception.HttpRequestTimeoutException
import com.amazonaws.regions.Region
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBStreams
import com.amazonaws.services.dynamodbv2.model.DescribeStreamRequest
import com.amazonaws.services.dynamodbv2.model.DescribeStreamResult
import com.amazonaws.services.dynamodbv2.model.ExpiredIteratorException
import com.amazonaws.services.dynamodbv2.model.GetRecordsRequest
import com.amazonaws.services.dynamodbv2.model.GetRecordsResult
import com.amazonaws.services.dynamodbv2.model.GetShardIteratorRequest
import com.amazonaws.services.dynamodbv2.model.GetShardIteratorResult
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement
import com.amazonaws.services.dynamodbv2.model.KeyType
import com.amazonaws.services.dynamodbv2.model.LimitExceededException
import com.amazonaws.services.dynamodbv2.model.ListStreamsRequest
import com.amazonaws.services.dynamodbv2.model.ListStreamsResult
import com.amazonaws.services.dynamodbv2.model.Record
import com.amazonaws.services.dynamodbv2.model.ResourceNotFoundException
import com.amazonaws.services.dynamodbv2.model.SequenceNumberRange
import com.amazonaws.services.dynamodbv2.model.ShardIteratorType
import com.amazonaws.services.dynamodbv2.model.StreamDescription
import com.amazonaws.services.dynamodbv2.model.StreamStatus
import com.amazonaws.services.dynamodbv2.model.StreamViewType
import com.amazonaws.services.dynamodbv2.model.TrimmedDataAccessException
import java.util.TreeMap
import java.util.UUID
import kotlin.random.Random

/**
 * A mock DynamoDB Streams client, which allows to simulate multiple
 * normal and failure scenarios.
 */
class MockDynamoDbStreams(
    private val streamArn: String
) : AmazonDynamoDBStreams {

    var isReachable = true
    var throttleAllShards = false
    var throttleDescribeStream = false

    private val shards = LinkedHashMap<Int, MockShard>()
    private var describeStreamsShardsPageLimit: Int = 10
    private val shardIterators: MutableMap<UUID, MockShardIterator> = HashMap()

    /**
     * Creates a shard with given [id], poiting to [parentId] as parent shard.
     * Adding shard with parent does not automatically create or close the parent
     * shard, nor validate that shard with [parentId] exists. It is okay if parent
     * shard does not exist.
     *
     * @throws IllegalStateException if shard with given ID already exists.
     */
    fun addShard(id: Int, parentId: Int?, records: List<MockRecord> = emptyList(), closed: Boolean = false) {
        check(!shards.containsKey(id)) { "Shard $id already exists" }
        shards[id] = MockShard(id, parentId)
        addRecordsToShard(id, records)
        if (closed) {
            closeShard(id)
        }
    }

    fun addRecordsToShard(shardId: Int, records: List<MockRecord>) {
        getShard(shardId).addRecords(records)
    }

    fun closeShard(shardId: Int) {
        getShard(shardId).close()
    }

    /**
     * Trims shard's data up to (excluding) given sequence number. For ease
     * of use, each shard starts with sequence number 0 (0-indexed) and
     * assigns sequence number greater by 1 to every next record. If you add
     * 3 records to shards, they will have following sequence numbers: 0, 1, 2.
     *
     * @throws IllegalArgumentException if given [untilSequenceNumber] is greater
     * than last record's sequence number.
     */
    fun trimShard(shardId: Int, untilSequenceNumber: Int) {
        getShard(shardId).trim(untilSequenceNumber)
    }

    fun expireIterators() {
        shardIterators.clear()
    }

    fun setDescribeStreamsShardsPageLimit(limit: Int) {
        describeStreamsShardsPageLimit = limit
    }

    override fun getShardIterator(getShardIteratorRequest: GetShardIteratorRequest): GetShardIteratorResult {
        checkReachable()

        if (getShardIteratorRequest.streamArn != streamArn) {
            throw ResourceNotFoundException("Unknown stream ${getShardIteratorRequest.streamArn}")
        }

        val shard = shards[getShardIteratorRequest.shardId.toInt()] ?: throw TrimmedDataAccessException(
            "Getting iterator for non-existing shard ${getShardIteratorRequest.shardId}"
        )

        val internalIterator = when (ShardIteratorType.fromValue(getShardIteratorRequest.shardIteratorType)!!) {
            ShardIteratorType.TRIM_HORIZON -> shard.getIteratorFromTrimHorizon()
            ShardIteratorType.LATEST -> shard.getIteratorFromLatest()
            ShardIteratorType.AFTER_SEQUENCE_NUMBER -> {
                shard.getIteratorAfterSequenceNumber(getShardIteratorRequest.sequenceNumber.toInt())
            }
            ShardIteratorType.AT_SEQUENCE_NUMBER -> throw NotImplementedError(
                "Iterator AT_SEQUENCE_NUMBER is not yet supported"
            )
        }

        val iterator = createAndGetIterator(shard.id, internalIterator)

        return GetShardIteratorResult()
            .withShardIterator(iterator.id.toString())
    }

    override fun getRecords(getRecordsRequest: GetRecordsRequest): GetRecordsResult {
        checkReachable()

        if (throttleAllShards) {
            throw LimitExceededException("Throttled")
        }

        val iteratorId = UUID.fromString(getRecordsRequest.shardIterator)
        val iterator = shardIterators[iteratorId] ?: throw ExpiredIteratorException(
            "Already expired or invalid shard iterator"
        )
        val shard = shards[iterator.shardId] ?: throw TrimmedDataAccessException("The shard was removed")
        val result = shard.getRecords(iterator.internalIterator, getRecordsRequest.limit)

        val nextIterator = result.nextIterator?.let { internalIterator ->
            createAndGetIterator(shard.id, internalIterator, prevIterator = iterator)
        }

        return GetRecordsResult()
            .withNextShardIterator(nextIterator?.id?.toString())
            .withRecords(result.records)
    }

    override fun describeStream(describeStreamRequest: DescribeStreamRequest): DescribeStreamResult {
        checkReachable()

        if (throttleDescribeStream) {
            val ex = AmazonServiceException("Throttled")
            ex.errorCode = "ThrottlingException"
            throw ex
        }

        if (describeStreamRequest.streamArn != streamArn) {
            throw ResourceNotFoundException("Unknown stream ${describeStreamRequest.streamArn}")
        }

        val exclusiveStartShardId = describeStreamRequest.exclusiveStartShardId?.toInt()
        val newShards = if (exclusiveStartShardId != null) {
            shards.asSequence().dropWhile { it.key != exclusiveStartShardId }.drop(1)
        } else shards.asSequence()

        val shardsLimit = describeStreamRequest.limit ?: describeStreamsShardsPageLimit
        val thisPageShards = newShards
            .take(shardsLimit)
            .map { (_, shard) ->
                com.amazonaws.services.dynamodbv2.model.Shard()
                    .withShardId(shard.id.toString())
                    .withParentShardId(shard.parentId?.toString())
                    .withSequenceNumberRange(
                        SequenceNumberRange()
                            .withStartingSequenceNumber("0")
                            .withEndingSequenceNumber(shard.getEndingSequenceNumber()?.toString())
                    )
            }
            .toList()

        // Return last evaluated shard ID if:
        // a. there are more shards to pull or
        // b. at random, if there are no more shards to pull
        // DynamoDB Streams usually doesn't return it if there is no next page, but it may
        // return it even if next page is empty (documented behavior)
        val hasNextPage = thisPageShards.size < newShards.count()
        val lastEvaluatedShardId = if (hasNextPage || Random.nextBoolean()) {
            thisPageShards.lastOrNull()?.shardId
        } else null

        return DescribeStreamResult().withStreamDescription(
            StreamDescription()
                .withStreamArn(streamArn)
                .withStreamStatus(StreamStatus.ENABLED)
                .withStreamViewType(StreamViewType.NEW_AND_OLD_IMAGES)
                .withKeySchema(KeySchemaElement(MockRecord.PARTITION_KEY_ATTR_NAME, KeyType.HASH))
                .withTableName("TestTable")
                .withLastEvaluatedShardId(lastEvaluatedShardId)
                .withShards(thisPageShards)
        )
    }

    override fun shutdown() {}

    override fun setRegion(region: Region) = throw NotImplementedError()

    override fun listStreams(listStreamsRequest: ListStreamsRequest): ListStreamsResult = throw NotImplementedError()

    override fun setEndpoint(endpoint: String) = throw NotImplementedError()

    override fun getCachedResponseMetadata(request: AmazonWebServiceRequest): ResponseMetadata =
        throw NotImplementedError()

    private fun getShard(shardId: Int): MockShard {
        return shards[shardId] ?: throw IllegalArgumentException("Shard $shardId doesn't exist")
    }

    /**
     * Creates or re-uses already existing iterator if it pointed to the same place.
     */
    private fun createAndGetIterator(
        shardId: Int,
        internalIterator: Int,
        prevIterator: MockShardIterator? = null
    ): MockShardIterator {
        val reuseIterator = prevIterator != null
            && prevIterator.shardId == shardId
            && prevIterator.internalIterator == internalIterator
        if (reuseIterator) {
            return prevIterator!!
        }

        val newIterator = MockShardIterator(UUID.randomUUID(), shardId, internalIterator)
        shardIterators[newIterator.id] = newIterator
        return newIterator
    }

    private fun checkReachable() {
        if (!isReachable) {
            throw AmazonClientException(HttpRequestTimeoutException("Unreachable"))
        }
    }

    /**
     * Simulates DynamoDB Shard. Not thread safe.
     */
    private class MockShard(
        val id: Int,
        val parentId: Int?
    ) {

        /**
         * Shard's records ordered and accessible by sequence number.
         */
        private val records = TreeMap<Int, MockRecord>()
        private var trimHorizonSequenceNumber: Int = 0
        private var latestSequenceNumber: Int = 0
        private var closed = false

        fun addRecords(records: List<MockRecord>) {
            check(!closed) { "Shard $id is already closed - can't add records" }

            var seqNumber = latestSequenceNumber
            records.forEach { record ->
                this.records[seqNumber] = record
                seqNumber++
            }
            latestSequenceNumber = seqNumber
        }

        /**
         * Closes shard, making adding records
         */
        fun close() {
            closed = true
        }

        fun getRecords(iterator: Int, limit: Int): MockGetRecordsResult {
            if (iterator < trimHorizonSequenceNumber) {
                throw TrimmedDataAccessException(
                    "The operation attempted to read past the oldest stream record in a shard.\n" +
                        "You obtained a shard iterator, but before you used it in a GetRecords request, " +
                        "a stream record in the shard is trimmed. This causes the iterator to access " +
                        "a record that no longer exists."
                )
            }

            val remainingRecords = records.tailMap(iterator, true)
            val recordsToReturn = remainingRecords
                .asSequence()
                .take(limit)
                .map { it.value.toDynamoDbStreamRecord(it.key) }
                .toList()

            val shardFinished = closed && remainingRecords.size == recordsToReturn.size

            return MockGetRecordsResult(
                recordsToReturn,
                if (!shardFinished) {
                    iterator + recordsToReturn.size
                } else null
            )
        }

        fun getIteratorFromTrimHorizon(): Int {
            return trimHorizonSequenceNumber
        }

        fun getIteratorFromLatest(): Int {
            return latestSequenceNumber
        }

        fun getIteratorAfterSequenceNumber(sequenceNumber: Int): Int {
            if (sequenceNumber < trimHorizonSequenceNumber) {
                throw TrimmedDataAccessException(
                    "The operation attempted to read past the oldest stream record in a shard."
                )
            }
            return sequenceNumber + 1
        }

        /**
         * Trims shard's data up to (excluding) given sequence number.
         * Given [untilSequenceNumber] cannot be greater than last record's
         * sequence number.
         */
        fun trim(untilSequenceNumber: Int) {
            require(untilSequenceNumber >= 0)
            require(untilSequenceNumber <= latestSequenceNumber) {
                "Given untilSequenceNumber cannot be greater than last record's sequence number"
            }

            trimHorizonSequenceNumber = untilSequenceNumber
        }

        fun getEndingSequenceNumber(): Int? = if (closed) latestSequenceNumber - 1 else null
    }

    private data class MockGetRecordsResult(
        val records: List<Record>,
        val nextIterator: Int?
    )

    private data class MockShardIterator(
        val id: UUID,
        val shardId: Int,
        val internalIterator: Int
    )
}