package com.nexxiot.kafka.connect.dynamodb

import io.mockk.every
import io.mockk.mockk
import org.apache.kafka.connect.source.SourceTaskContext
import org.apache.kafka.connect.storage.OffsetStorageReader

class MockSourceTaskContext : SourceTaskContext {

    private var offsetReaderMock = mockk<OffsetStorageReader>() {
        every { offset<String>(any()) } returns null
    }

    fun <T, O : Any?> setOffset(partition: Map<String, T>, offset: Map<String, O>) {
        every { offsetReaderMock.offset(partition) } returns offset
    }

    override fun offsetStorageReader(): OffsetStorageReader {
        return offsetReaderMock
    }

    override fun configs(): MutableMap<String, String> {
        throw NotImplementedError("Wasn't needed so far")
    }
}