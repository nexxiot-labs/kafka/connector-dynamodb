package com.nexxiot.kafka.connect.dynamodb

import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.client.builder.AwsClientBuilder
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder
import com.amazonaws.services.dynamodbv2.document.DynamoDB
import com.amazonaws.services.dynamodbv2.document.Item
import com.amazonaws.services.dynamodbv2.model.AttributeDefinition
import com.amazonaws.services.dynamodbv2.model.BillingMode
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest
import com.amazonaws.services.dynamodbv2.model.KeySchemaElement
import com.amazonaws.services.dynamodbv2.model.KeyType
import com.amazonaws.services.dynamodbv2.model.ScalarAttributeType
import com.amazonaws.services.dynamodbv2.model.StreamSpecification
import com.amazonaws.services.dynamodbv2.model.StreamViewType
import com.amazonaws.services.dynamodbv2.util.TableUtils
import com.fasterxml.jackson.databind.ObjectMapper
import io.kotlintest.TestCase
import io.kotlintest.TestResult
import io.kotlintest.TestStatus
import io.kotlintest.matchers.string.shouldNotBeEmpty
import io.kotlintest.should
import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import io.ktor.client.HttpClient
import io.ktor.client.request.post
import io.ktor.client.response.HttpResponse
import io.ktor.content.TextContent
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import org.apache.kafka.clients.admin.AdminClient
import org.apache.kafka.clients.admin.AdminClientConfig
import org.apache.kafka.clients.admin.NewTopic
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.clients.consumer.ConsumerRecords
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.common.TopicPartition
import org.apache.kafka.common.serialization.Serdes
import org.testcontainers.containers.KafkaContainer
import org.testcontainers.containers.Network
import java.time.Duration

class IntegrationTest : StringSpec() {

    // Containers need to be on the same network to contact each other.
    private val network: Network = Network.newNetwork()

    private val kafkaContainer: KafkaContainer = autoClose(
        KafkaContainer("5.3.3")  // 5.3 is Kafka 2.3
            .withNetwork(network)
            .withNetworkAliases("kafka")
    )

    private val dynamoDbContainer: DynamoDbContainer = autoClose(
        DynamoDbContainer()
            .withNetwork(network)
            .withNetworkAliases("dynamodb")
    )

    private val kafkaConnectContainer: KafkaConnectContainer = autoClose(
        KafkaConnectContainer("kafka:9092")
            .withNetwork(network)
    )

    private val httpClient: HttpClient = autoClose(HttpClient())
    private val json: ObjectMapper = ObjectMapper()

    init {
        kafkaContainer.start()
        kafkaConnectContainer.start()
        dynamoDbContainer.start()

        val kafkaAdmin: AdminClient = autoClose(
            AdminClient.create(
                mapOf(
                    AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG to kafkaContainer.bootstrapServers
                )
            )
        )

        val kafkaConsumer = autoClose(
            KafkaConsumer<String?, String?>(
                mapOf(
                    ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG to kafkaContainer.bootstrapServers,
                    ConsumerConfig.AUTO_OFFSET_RESET_CONFIG to "earliest"
                ),
                Serdes.String().deserializer(),
                Serdes.String().deserializer()
            )
        )

        "DynamoDB Connector - Data is written to DynamoDB table - Changes are pushed to Kafka" {
            val dynamoDbEndpoint = "http://${dynamoDbContainer.containerIpAddress}:${dynamoDbContainer.mappedPort}"
            val dbClient = AmazonDynamoDBClientBuilder.standard()
                .withEndpointConfiguration(AwsClientBuilder.EndpointConfiguration(dynamoDbEndpoint, "us-east-1"))
                .withCredentials(AWSStaticCredentialsProvider(BasicAWSCredentials("dummy-key", "dummy-secret")))
                .build()

            val testTopicName = "dynamo_db.users_table.stream"
            kafkaAdmin.createTestTopic(testTopicName)

            // Create table with stream enabled
            val testTableName = "Users"
            val keyIdField = "id"
            dbClient.createTable(
                CreateTableRequest()
                    .withTableName(testTableName)
                    .withAttributeDefinitions(
                        listOf(
                            AttributeDefinition().withAttributeName(keyIdField).withAttributeType(
                                ScalarAttributeType.S
                            )
                        )
                    )
                    .withKeySchema(
                        KeySchemaElement().withAttributeName(keyIdField).withKeyType(KeyType.HASH)
                    )
                    .withBillingMode(BillingMode.PAY_PER_REQUEST)
                    .withStreamSpecification(
                        StreamSpecification()
                            .withStreamViewType(StreamViewType.NEW_AND_OLD_IMAGES)
                            .withStreamEnabled(true)
                    )
            )

            @Suppress("BlockingMethodInNonBlockingContext")
            TableUtils.waitUntilActive(dbClient, testTableName)
            val tableStreamArn = dbClient.describeTable(testTableName).table.latestStreamArn

            kafkaConnectContainer.createConnector(
                """
                    {
                      "name": "dynamodb-connector",
                      "config": {
                        "connector.class": "com.nexxiot.kafka.connect.dynamodb.DynamoDbSourceConnector",
                        "tasks.max": "1",
                        "key.converter": "org.apache.kafka.connect.storage.StringConverter",
                        "value.converter": "org.apache.kafka.connect.json.JsonConverter",
                        "value.converter.schemas.enable": false,
                        "streams.arn": "$tableStreamArn",
                        "kafka.topic": "$testTopicName",
                        "streams.initial.position": "TRIM_HORIZON",
                        "streams.get.records.min.interval.ms": 100,
                        "streams.mock.endpoint.url": "http://dynamodb:${DynamoDbContainer.DYNAMO_DB_PORT}"
                      }
                    }
                """.trimIndent()
            )

            // Add arbitrary entries to the table
            val testTableItem = Item().withPrimaryKey(keyIdField, "john")
                .withString("name", "John Smith")
                .withNumber("age", 44)
            DynamoDB(dbClient).getTable(testTableName).putItem(testTableItem)

            val records = kafkaConsumer.pollTestTopic(testTopicName, Duration.ofSeconds(30))
            records.count() shouldBe 1

            val kafkaRecord = records.first()

            kafkaRecord.key() shouldBe "john"
            val valueJson = json.readTree(kafkaRecord.value()!!)
            valueJson.fieldNames().asSequence().toSet() shouldBe setOf(
                "id",
                "type",
                "approx_time",
                "keys",
                "new_image",
                "old_image",
                "discover_time"
            )
            valueJson["approx_time"].asText().shouldNotBeEmpty()
            valueJson["type"].textValue() shouldBe "INSERT"
            valueJson["keys"].textValue() should equalJson("""{"id": "john"}""")

            valueJson["new_image"].textValue() should equalJson(
                """
                    {
                      "id": "john",
                      "name": "John Smith",
                      "age": 44
                    }
                """.trimIndent()
            )
        }
    }

    private suspend fun KafkaConnectContainer.createConnector(body: String): HttpResponse {
        val response = httpClient.post<HttpResponse>(
            host = containerIpAddress,
            port = restApiPort,
            path = "/connectors/",
            body = TextContent(body, ContentType.Application.Json)
        )
        response.status shouldBe HttpStatusCode.Created
        return response
    }

    private fun AdminClient.createTestTopic(name: String) {
        @Suppress("BlockingMethodInNonBlockingContext") createTopics(
            listOf(
                NewTopic(name, 1, 1)
            )
        ).all().get()
    }

    private fun KafkaConsumer<String?, String?>.pollTestTopic(
        topicName: String,
        timeout: Duration
    ): ConsumerRecords<String?, String?> {
        assign(listOf(TopicPartition(topicName, 0)))
        return poll(timeout)
    }

    override fun afterTest(testCase: TestCase, result: TestResult) {
        if (result.status == TestStatus.Failure || result.status == TestStatus.Error) {
            val tailSize = 300
            println("==> KAFKA CONNECT LOGS TAIL (up to $tailSize lines) =====")

            val logLines = kafkaConnectContainer.logs.split('\n')
            if (logLines.size > tailSize) {
                println("...")
            }

            logLines
                .filter { it.isNotEmpty() }
                .takeLast(tailSize)
                .forEach { println(it) }

            println()
            println("=== END KAFKA CONNECT LOGS ==================")
        }
    }
}