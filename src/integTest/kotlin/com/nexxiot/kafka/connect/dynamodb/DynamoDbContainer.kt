package com.nexxiot.kafka.connect.dynamodb

import org.testcontainers.containers.GenericContainer

class DynamoDbContainer : GenericContainer<DynamoDbContainer>("amazon/dynamodb-local") {
    companion object {
        const val DYNAMO_DB_PORT = 8000
    }

    init {
        withExposedPorts(DYNAMO_DB_PORT)
    }

    val mappedPort: Int
        get() = getMappedPort(DYNAMO_DB_PORT)
}