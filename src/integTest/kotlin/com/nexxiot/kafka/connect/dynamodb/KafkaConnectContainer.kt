package com.nexxiot.kafka.connect.dynamodb

import org.testcontainers.containers.GenericContainer

class KafkaConnectContainer(
    kafkaBootstrapServers: String
) : GenericContainer<KafkaConnectContainer>("nexxiot-kafka-connector-dynamodb") {
    companion object {
        private const val REST_API_PORT = 8083
    }

    init {
        withExposedPorts(REST_API_PORT)
        withEnv("CONNECT_BOOTSTRAP_SERVERS", kafkaBootstrapServers)
        withEnv("CONNECT_GROUP_ID", "test-connect")
        withEnv("CONNECT_CONFIG_STORAGE_TOPIC", "test-connect-configs")
        withEnv("CONNECT_OFFSET_STORAGE_TOPIC", "test-connect-offsets")
        withEnv("CONNECT_STATUS_STORAGE_TOPIC", "test-connect-statuses")
        withEnv("CONNECT_OFFSET_STORAGE_REPLICATION_FACTOR", "1")
        withEnv("CONNECT_CONFIG_STORAGE_REPLICATION_FACTOR", "1")
        withEnv("CONNECT_STATUS_STORAGE_REPLICATION_FACTOR", "1")
        withEnv("CONNECT_KEY_CONVERTER", "org.apache.kafka.connect.json.JsonConverter")
        withEnv("CONNECT_KEY_CONVERTER_SCHEMAS_ENABLE", "false")
        withEnv("CONNECT_VALUE_CONVERTER", "org.apache.kafka.connect.json.JsonConverter")
        withEnv("CONNECT_VALUE_CONVERTER_SCHEMAS_ENABLE", "false")
        withEnv("CONNECT_REST_ADVERTISED_HOST_NAME", "localhost")
        withEnv("CONNECT_OFFSET_FLUSH_INTERVAL_MS", "10000")
    }

    val restApiPort: Int
        get() = getMappedPort(REST_API_PORT)
}