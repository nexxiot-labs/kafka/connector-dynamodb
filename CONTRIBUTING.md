Contributing
============

We welcome anyone that wants to help out in any way, whether that includes
reporting problems, helping with documentation, or contributing code changes
to fix bugs, add tests, or implement new features. Here are a few important
things you should know about contributing:

* New features or interface changes require discussion, use cases, etc. Code
  comes later.
* Merge requests are great for small fixes for bugs, documentation, etc.
* By contributing, you agree that your contributions will be licensed under
  project's license (see DCO below).


Our development process
-----------------------

We use GitLab to track [issues][issue-tracker] and feature requests, as well
as accept [merge requests][merge-requests].

[issue-tracker]: https://gitlab.com/nexxiot-labs/kafka/connector-dynamodb/-/issues
[merge-requests]: https://gitlab.com/nexxiot-labs/kafka/connector-dynamodb/-/merge_requests


Developer's Certificate of Origin 1.1
-------------------------------------

By making a contribution to this project, I certify that:

(a) The contribution was created in whole or in part by me and I have the right
to submit it under the open source license indicated in the file; or

(b) The contribution is based upon previous work that, to the best of my
knowledge, is covered under an appropriate open source license and I have the
right under that license to submit that work with modifications, whether created
in whole or in part by me, under the same open source license (unless I am
permitted to submit under a different license), as indicated in the file; or

(c) The contribution was provided directly to me by some other person who
certified (a), (b) or (c) and I have not modified it.

(d) I understand and agree that this project and the contribution are public
and that a record of the contribution (including all personal information I
submit with it, including my sign-off) is maintained indefinitely and may be
redistributed consistent with this project or the open source license(s)
involved.
